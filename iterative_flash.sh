#! /bin/bash

# This script takes two paired end read files and trys them to merge
# For all reads who could no be merged, it then performs iterative quality clipping and trys again, iteratively 
# This script is expected to be run from the amplicon_miseq pipeline, if not, then it performs some stat inferring first

source $(dirname `readlink -f $0`)/build_environment
combinedMin=98; # this is the maximum of allowed un-merged pairs
clipQualityMAX=36; #if we reach this, stop clipping
clipQualityPEStep=5;

#1 USAGE
function usage {
    echo -e "\n  Merge two PE raids using flash\n"
    echo -e "\n  Usage: $0 -f <forward_reads.fastq> -r <reverse_reads.fastq> .fastq> -l <mean_read_length> -s <fragment_size> -S <fragment_stdv> -m <max_overlap> -q <q_score_trim_step>" 
    echo -e "\t-f  path to the forward reads file (FASTQ format)"
    echo -e "\t-r  path to the reverse reads file (FASTQ format)"
    echo -e "\t-l  inferred average read length. If omitted, it will be estimated from data."
    echo -e "\t-s  inferred fragment size of the amplicon. If omitted, it will be estimated from data."
    echo -e "\t-S  inferred fragment standard deviation of the amplicon. If omitted, it will be estimated from data."
    echo -e "\t-m  max allowed overlap for the PE reads. If omitted, none will be used. If given, fragment size and stdv will not be used."
    echo -e "\t-q  q-score step size. Quality trimming starts at q-score 20 and increases for the given step size. Default is 2."
    exit 1
}

while getopts 'f:r:l:s:S:m:q:h' opt
do
   case $opt in
    f) inputF=$OPTARG;;
    r) inputR=$OPTARG;;
    l) readLen=$OPTARG;;
    s) fragSize=$OPTARG;;
    S) fragStdv=$OPTARG;;
    m) maxOlap=$OPTARG;;
    q) clipQualityPEStep=$OPTARG;;
    h) usage;;
    :)
        echo "Option -$OPTARG requires an argument." >&2
        exit 1
        ;;
    \?)
        echo "Invalid option: -$OPTARG" >&2
        exit 1
        ;;
    esac
done

## providing info about passed arguments
echo -e "[CLI] Started pe merging with the following parameters:
    f= $inputF
    r= $inputR
    l= $readLen 
    s= $fragSize 
    S= $fragStdv
    m= $maxOlap
    q= $clipQualityPEStep
		"	
#### Sanity checks on parameters #####
## 1: Input
if [ "a${inputF}" == "a" -a "a${inputR}" != "a" ]; then
    echo -e "[CLI] No forward input given. Aborting!"
    usage;
fi
if [ "a${inputF}" != "a" -a "a${inputR}" == "a" ]; then
    echo -e "[CLI] No reverse input given. Aborting!"
    usage;
fi
if [ "a${inputF}" == "a" -a "a${inputR}" == "a" ]; then
    echo -e "[CLI] No input given. Aborting!"
    usage;
fi
if [ "a${inputF}" != "a" -a "${inputF}" == "${inputB}" ]; then
    echo -e "[CLI] Forward and reverse reads are equal. Two (separate) input files have to be given. Aborting!"
    usage;
fi
if [ "a${inputF}" != "a" -a ! -r "${inputF}" ]; then
    echo -e "[CLI] Cannot read seqeuncing file: ${inputF}.Aborting!"
    exit 1
else
    inputF=`realpath ${inputF}`
fi
if [ "a${inputR}" != "a" -a ! -r "${inputR}" ]; then
    echo -e "[CLI] Cannot read seqeuncing file: ${inputR}.Aborting!"
    exit 1
else
    inputR=`realpath ${inputR}`
fi

####merge pairs####
if [ "$fragSize" == "" -o "$fragStdv" == "" -o "$readLen" != "" ]; then
    echo -e "\n"
    echo -e "[MERGE] Inferring read length from input ..."
    inputFHist=`basename ${inputF}`.hist
    inputRHist=`basename ${inputR}`.hist
  	echo -e "[MERGE] Calling $perl ${scriptDir}/FastaStats.pl -q ${inputF} > ${inputFHist}"
   	echo -e "[MERGE] Calling $perl ${scriptDir}/FastaStats.pl -q ${inputR} > ${inputRHist}"
    $perl ${scriptDir}/FastaStats.pl -q ${inputF} > ${inputFHist}
    $perl ${scriptDir}/FastaStats.pl -q ${inputR} > ${inputRHist}
    readLen=`$grep "Mean length" ${inputFHist} | awk '{printf "%s", $3}'`
    readLen=`printf '%0.f' $readLen`
	echo -e "[MERGE] Estimating Fragment size from read length: $readLen"
	fragSize=`perl -e "printf \"%.f\", ($readLen * 2) - (($readLen * 2 * 10) / 100)"`
	fragStdv=`perl -e "printf \"%.f\", ($fragSize/10)"` 
	echo -e "[MERGE] Fragment size = $fragSize ; stdv = $fragStdv"
fi
if [ "a${maxOlap}" == "a" ]; then
	flashParam="-f ${fragSize} -s ${fragStdv} -r ${readLen} "
else
	flashParam="-M ${maxOlap} "
fi
flashParam="${flashParam} -t $numProc"

echo -e "[MERGE] Merging forward and reverse reads ..."
echo -e "[MERGE] executing: ${binDir}/flash ${inputF} ${inputR} ${flashParam} -o merged "

flashOutput=`${binDir}/flash ${inputF} ${inputR} ${flashParam} -o merged 2>&1`

echo -e "$flashOutput"
echo -e "[MERGE] ... done."
mv merged.extendedFrags.fastq merged.extendedFrags_IT.fastq

# check if we have unmerged pairs
total=`echo -e "$flashOutput" | grep "Total" | $awk '{printf "%s", $4}'`
combined=`echo -e "$flashOutput" | grep "Combined" | $awk '{printf "%s", $4}'`
percC=`perl -e "printf \"%.f\", (($combined/$total)*100)"`
percC_old=$percC

while [ ${percC} -lt $combinedMin ] && [ $clipQualityPE -lt $clipQualityMAX ]; do
	##### Perform quality clipping before merging####
	inputFF=`realpath merged.notCombined_1.fastq`
	inputRR=`realpath merged.notCombined_2.fastq`
	echo -e "[CLIP-PE] Perform PE quality clipping before merging for failed reads"
    echo -e "[CLIP-PE] Clip to average quality level ..."
	echo -e "[CLIP-PE] Executing: ${binDir}sickle pe -f ${inputFF} -r ${inputRR} -t sanger -o forward.clipped_pe.fastq -p reverse.clipped_pe.fastq -q ${clipQualityPE} -x -s single.clipped_pe.fastq"
    ${binDir}sickle pe -f ${inputFF} -r ${inputRR} -t sanger -o forward.clipped_pe.fastq -p reverse.clipped_pe.fastq -q ${clipQualityPE} -x -s single.clipped_pe.fastq
	echo -e "[CLIP-PE] ... done."
	if [ ! -s forward.clipped_pe.fastq ] || [ ! -s reverse.clipped_pe.fastq ]; then
		break
	fi
	# we need to adopt the flash arguments
	if [ "a${maxOlap}" == "a" ]; then
  		echo -e "[CLIP-PE-MERGE] Calling $perl ${scriptDir}/FastaStats.pl -q forward.clipped_pe.fastq > forward.clipped_pe.hist"
	  	echo -e "[CLIP-PE-MERGE] Calling $perl ${scriptDir}/FastaStats.pl -q reverse.clipped_pe.fastq > reverse.clipped_pe.hist"
    	$perl ${scriptDir}/FastaStats.pl -q forward.clipped_pe.fastq > forward.clipped_pe.hist
	    $perl ${scriptDir}/FastaStats.pl -q reverse.clipped_pe.fastq > reverse.clipped_pe.hist
	    readLenF=`$grep "Mean length" forward.clipped_pe.hist | awk '{printf "%s", $3}'`
    	readLenR=`$grep "Mean length" reverse.clipped_pe.hist | awk '{printf "%s", $3}'`
	    readLen=`perl -e "printf \"%.f\", (($readLenF+$readLenR) /2)"`
		echo -e "[CLIP-PE-MERGE] Estimating Fragment size from read length $readLen"
		fragSize=`perl -e "printf \"%.f\", ($readLen * 2) - (($readLen * 2 * 10) / 100)"`
		fragStdv=`perl -e "printf \"%.f\", ($fragSize/10)"` 
		echo -e "[CLIP-PE-MERGE] Fragment size = $fragSize ; stdv = $fragStdv"
		flashParam="-f ${fragSize} -s ${fragStdv} -r ${readLen} "
	else
		flashParam="-M ${maxOlap} "
	fi
	flashParam="${flashParam} -t $numProc"
	flashParam="forward.clipped_pe.fastq reverse.clipped_pe.fastq ${flashParam}"
	echo -e "[MERGE-IT] executing: ${binDir}/flash ${flashParam} -o merged "
	flashOutput=`${binDir}/flash ${flashParam} -o merged 2>&1`
	echo -e "$flashOutput"
	echo -e "[MERGE-IT] ... done."
	# check if we have unmerged pairs
	combined_IT=`echo -e "$flashOutput" | grep "Combined" | $awk '{printf "%s",$4}'`
	combined=$(($combined + $combined_IT))
	percC=`perl -e "printf \"%.f\", (($combined/$total)*100)"`
	echo "[MERGE-IT] Increased merging to: $percC percent ($combined_IT / $total)"
	clipQualityPE=$(($clipQualityPE + $clipQualityPEStep))
	$cat merged.extendedFrags.fastq >> merged.extendedFrags_IT.fastq
done
echo -e "[MERGE] Finished PE merging. Final ration is: $percC (without clipping it was $percC_old)"
rm single.clipped_pe.fastq 
rm forward.clipped_pe.fastq
rm reverse.clipped_pe.fastq
rm forward.clipped_pe.hist
rm reverse.clipped_pe.hist
mv merged.extendedFrags_IT.fastq merged.extendedFrags.fastq

#############################

exit;
