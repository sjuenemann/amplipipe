#! /bin/bash

SGE_PROJECT=denbi

function usage {
    echo -e "\n  THIS script starts one of the amplicon MiSeq EASY pipelines while iterating over an input lists which are giving the input data pairs and the used primers"
    echo -e "\n  Usage: $0 -i <input_list> -s <path_to_script> -c -h -p <parameters>" 
    echo -e "\n"
    echo -e "\t-i  : a file in the form of
		    /path/to/forward/reads\t/path/to/reverse/reads\t/path/to/primers.file"
    echo -e "\t-s  : the scipt which should be used either:
		    /usr/local/AmpliconPipe/run_ampli_pipe_EASY_onMOCK.sh OR
		    /usr/local/AmpliconPipe/run_ampli_pipe_EASY_deNovo.sh" 
    echo -e "\n\t Hint: An easy way to construct the <input_list> is using the linux find command. If e.g. all forward and reverse pairs of input data of several e.g. MiSeq runs are located beneath a specific folder"
    echo -e "\t one example could look like:"
    echo -e "\t /> cd  /path/to/dir/with/input/datas"
    echo -ne "\t"; echo -E " /> find \$PWD -iname \"*R1*.fastq\" -print | perl -ne 'chomp;(\$t=\$_)=~s/R1/R2/g; printf(\"%s\\t%s\\t%s\\n\",\$_,\$t,\"/usr/local/AmpliconPipe/primers_example.txt\");' > input_list.tsv"
    echo -e "\t the string /usr/local/AmpliconPipe/primers_example.txt should be replaced with the appropriate primer.txt file location"
    echo -e "\t-c  : runs all individual amplicon pipe jobs on the cluster" 
    echo -e "\t-p  : additional parameters to be passed to the running script. needs to be put in single quotes" 
}


while getopts 'i:s:p:hc' opt
do
   case $opt in
    i) input=$OPTARG;;
    s) script=$OPTARG;;
    c) cluster=1;;
	p) params=$OPTARG;;
    h) usage;;
    \?)
        echo "Invalid option: -$OPTARG" >&2
        exit 1
        ;;
    :)
        echo "Option -$OPTARG requires an argument." >&2
        exit 1
        ;;
    esac
done

if [  "a${script}" == "a" -o ! -r "${script}" ]; then
    echo -e " Cannot read the script file: $script. Aborting!"
    usage;
    exit 1;
fi

jobN=`wc -l ${input} | awk '{print $1}'`
counter=1
while read inLine; do
    inA=`echo $inLine | awk '{print $1}'`
    inB=`echo $inLine | awk '{print $2}'`
    primers=`echo $inLine | awk '{print $3}'`
    if [  "a${inA}" == "a" -o ! -r "${inA}" ]; then
	echo -e " Cannot read first input data: $inA. Aborting!";
	exit 1;
    fi
    if [  "a${inB}" == "a" -o ! -r "${inB}" ]; then
	echo -e " Cannot read second input data: $inB. Aborting!";
	exit 1;
    fi
    if [  "a${primers}" == "a" -o ! -r "${primers}" ]; then
	echo -e " Cannot read primer file: $primers. Aborting!";
	exit 1;
    fi
    echo -e "[$counter] Processing the $counter line of $input"
    echo -e "[$counter] Executing: $script $inA $inB $primers $params ..."
	if [ "${cluster}" == "1" ]; then
	    #qsub -N amplicon_pipeline -wd $PWD -o $PWD/run_multiple_pipes.log -j y -l arch=lx-amd64 -l vf=2G -pe multislot 24 -t 1-${jobN} $script $inA $inB $primers
	    qsub -P ${SGE_PROJECT} -N amplicon_pipeline_${counter} -wd $PWD -o $PWD/run_multiple_pipes_${counter}.log -j y -l arch=lx-amd64 -l vf=1G -pe multislot 2 -l h="!fozzie" $script $inA $inB $primers $params
	else
	    $script $inA $inB $primers $params
	fi
    echo -e "[$counter] ... done."
    counter=$(($counter+1))
done < $input




