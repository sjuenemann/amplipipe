#! /bin/bash

# This script executes takes MiSeq paired-end data (seperate, interleaved, merged) and performs several 16S rRNA analysis steps (utilizing mothur/USEARCH) on them


#1 USAGE
function usage {
    echo "Usage missing"
}

useTUIT=0;
useUCHIMEREF=0
configFile=$(dirname `readlink -f $0`)/build_environment

while getopts 'i:o:C:h' opt
do
   case $opt in
    i) input=$OPTARG;;
    o) outDir=$OPTARG;;
	C) configFile=$OPTARG;;
    h) usage;;
    :)
        echo "Option -$OPTARG requires an argument." >&2
        exit 1
        ;;
    \?)
        echo "Invalid option: -$OPTARG" >&2
        exit 1
        ;;
    esac
done

source $configFile

#### Sanity checks on parameters #####
## 1: Input
if [  "a${input}" == "a" ]; then
    echo -e "[CLI] No input given. Provide a directory containg fasta and count files for each sample. Aborting!"
    usage;
fi
if [  "a${input}" != "a" -a ! -d "${input}" ]; then
    echo -e "[CLI] Cannot read input dir: ${input}.Aborting!"
    exit 1
else
    input=`realpath ${input}`
fi

####create outdir if given####
if [ "a${outDir}" != "a" ]; then
    if [ ! -d ${outDir} ]; then
    	echo -e "[OUT] Output directory given. Creating output directory: ${outDir}"
	mkdir ${outDir}
    else 
    	echo -e "[OUT] Output directory given. Using ${outDir}"
    fi
    echo -e "[OUT] Switching to output_dir: ${outDir}."
    cd ${outDir}
    outDir=`pwd`
else
    echo -e "[OUT] Output directory not given. Using current directory: ${PWD}"
    outDir=${PWD}
fi
#############################

fastaEntries=(`ls ${input}/*.fasta`)
allEntries=""
allGroups=""
counter=0
touch GROUPS
for entry in ${fastaEntries[@]};
do
    counter=$(($counter+1))
    if [ "$allEntries" == "" ]; then
	allEntries=$entry
	allGroups=G$counter
    else
	allEntries="${allEntries}-$entry";
	allGroups="${allGroups}-G$counter"
    fi
    echo -e "`basename $entry .fasta`\tG$counter" >> GROUPS
done

####Now switch using mothur for the real amplicon pipeline####
    echo -e "\n\n"
    echo -e "[MOTHUR] Statring amplicon pipeline parts"
    echo -e "[MOTHUR] Creating mothur output folder: ${outDir}/mothur_pipeline and switching to it"
    mothurOut=${outDir}/mothur_pipeline
    mkdir ${mothurOut}
    mothurLogs=${outDir}/mothur_pipeline/mothur_logs
    mkdir ${mothurLogs}
    inBase="grouped"

    echo -e "\n"
    echo -e "[MOTHUR] ** creating groups file **"
    ${mothur} "#set.logfile(name=${mothurLogs}/make.group.log);make.group(fasta=${allEntries}, groups=${allGroups})" 
	groupsFile=`grep "Output File Names:" ${mothurLogs}/make.group.log | awk '{print $4}'`
    #groupsFile=${allEntries//fasta/}
    #groupsFile=${groupsFile//-/}groups
	#if [ ! -f ${groupsFile} ]; then
#		groupsFile="mergegroups"
#	fi    
	mv ${groupsFile} ${mothurOut}

    echo -e "\n"
    echo -e "[MOTHUR] ** combining all FASTA and count tables into one **"
    cat ${input}/*.fasta > ${mothurOut}/${inBase}.fasta
    cat ${input}/*.taxonomy > ${mothurOut}/${inBase}.taxonomy
    echo -e "Representative_Sequence\ttotal\t${allGroups}" | tr '-' "\t" > ${mothurOut}/${inBase}.counts
    counter2=0
    for entry in ${fastaEntries[@]};
    do
		counter2=$(($counter2+1))
		groupParts=""
		for counts in `seq 1 $counter`;
		do
	    	if [[ $counts -eq $counter2 ]]; then
				groupParts="$groupParts\tXXXXX"
	    	else
				groupParts="$groupParts\t0"
	    	fi
		done
		tail -qn +2 ${entry%.fasta}.count_table | $awk -v my_var="$groupParts" '{print $1," ",$2," ",my_var}' | perl -ne '@v=split; $a=join("\t",$_); $a =~ s/XXXXX/$v[1]/; print "$a"' >> ${mothurOut}/${inBase}.counts
    done

    cd ${mothurOut}
	if [ "${reClass}" == "1" ]; then
		if [ "${useTUIT}" == "1" ]; then
            mv ${inBase}.taxonomy ${inBase}.taxonomy.old
		    echo -e "\n"
		    echo -e "[TUIT] ** TUIT classification enbaaled **"
		    echo -e "[TUIT] ** classify sequences using TUIT and reference DB: ${referenceTUIT}**"
		    mkdir ${outDir}/tuit
		    cd ${outDir}/tuit
		    ln -s ${outDir}/mothur_pipeline/${inBase}.fasta
		    ln -s ${outDir}/mothur_pipeline/${inBase}.counts
		    mkdir ${outDir}/tuit/splitted
		    cd ${outDir}/tuit/splitted
		    totNums=`$grep -c \> ../${inBase}.fasta`
		    seqsPerFile=`perl -e "printf(\"%.0f\", $totNums/$numProc)"`
		    echo -e "[TUIT] splitting FASTA file into sub-parts: $seqsPerFile sequences per file = for $numProc total files"
		    ${scriptDir}/split_multifasta.pl --input_file ../${inBase}.fasta --output_dir=$PWD --seqs_per_file=$seqsPerFile
		    parts=`ls *.fsa`
		    echo -e "[TUIT] running BLAST on each splitted FASTA and TUIT afterwards ... "
		    echo -e "[TUIT] executing: parallel --gnu ${scriptDir}/run_blast_tuit.sh {} ${referenceTUIT} ${referenceGIL} ${EVALUE} 1 ::: ${parts} >> ${outDir}/tuit/${inBase}.unique.good.filter.unique.precluster.pick.tuit"
		    parallel --gnu ${scriptDir}/run_blast_tuit.sh {1} ${referenceTUIT} ${referenceGIL} ${EVALUE} 1 ::: ${parts} >> ${outDir}/tuit/${inBase}.tuit
		    echo -e "[TUIT] ... done"
		    cd ..
		    ${scriptDir}/tuit_output2mothur_readable.sh ${inBase}.tuit ${inBase}.counts
		    cd ${outDir}/mothur_pipeline
		    cp ${outDir}/tuit/${inBase}.tax ${inBase}.taxonomy
		    echo -e "[TUIT] finished TUIT. Results are: ${outDir}results/${outPrefix}_TUIT.tax \t \n\n and  ${outDir}results/${outPrefix}_TUIT.tax.summary"
		    cd ${outDir}
		else
			# classify sequences using 
			mv ${inBase}.taxonomy ${inBase}.taxonomy.old
	    	echo -e "\n"
		    echo -e "[MOTHUR] ** classify sequences using bayesian approch according to reference DB: ${referenceClassFas} and ${referenceClassTax} **"
    		${mothur} "#set.logfile(name=mothur_logs/mothur.classify.log);classify.seqs(fasta=${inBase}.fasta, count=${inBase}.counts, template=${referenceClassFas}, taxonomy=${referenceClassTax}, cutoff=$classCutOff, processors=$numProc, probs=$classParams )"
			newTaxFile=`find . -name "*.taxonomy"`
			mv $newTaxFile ${inBase}.taxonomy
		fi
	fi


    ${mothur} "#set.logfile(name=${mothurLogs}/unique.seqs.log);unique.seqs(fasta=${inBase}.fasta, count=${inBase}.counts)"

    # cluster OTUs
    # clustering with mothur takes to long or doesnt work at all, we will do this with usearch
    # first convert fasta to usearch format
    #${scriptDir}/AppendCountTableToFastA.pl ${inBase}.unique.good.filter.unique.precluster.pick.fasta ${inBase}.unique.good.filter.unique.precluster.pick.count_table > ${inBase}.cheked.count.fasta
    ${scriptDir}/AppendCountTableToFastA.pl ${inBase}.unique.fasta ${inBase}.count_table > ${inBase}.cheked.count.fasta
    ${scriptDir}/clean_fasta.py -f ${inBase}.cheked.count.fasta
    mv ${inBase%%.highQual}_filtered.fasta ${inBase}.cheked.count.fasta 
    sed -i "-es/^>\(.*\)/>\1;barcodelabel=mock1;/" ${inBase}.cheked.count.fasta
    ${usearch} -sortbysize ${inBase}.cheked.count.fasta -fastaout ${inBase}.cheked.count.sorted.fasta -minsize 2 
    ${usearch} -cluster_otus ${inBase}.cheked.count.sorted.fasta -otus ${inBase}.cheked.count.sorted.OTUs.fasta -uparseout ${inBase}.cheked.count.sorted.OTUs.out -sizein -sizeout
    #${usearch} -cluster_otus ${inBase}.cheked.count.sorted.fasta -otus ${inBase}.OTUs.centroids.fasta -sizein -sizeout -relabel OTU
    ${usearch} -usearch_global ${inBase}.cheked.count.fasta -db ${inBase}.cheked.count.sorted.OTUs.fasta -strand plus -id $clusteringPercentIdentCutOff -uc ${inBase}.checked.count.OTUs.map.uc
    ${scriptDir}/ConvertUSEARCH2MothurList.pl ${inBase}.checked.count.OTUs.map.uc ${inBase}.count_table ${inBase}.cheked.count.sorted.OTUs.fasta ${inBase}.OTUs.list ${inBase}.OTUs.centroids.fasta 

    #classify OTUs
    echo -e "\n"
    echo -e "[MOTHUR] ** classify OTUs based on read level classification **"
    taxonomyFile=${inBase}.taxonomy
    #${mothur} "#set.logfile(name=${mothurLogs}/mothur.classify.OTUs.log);classify.otu(list=${inBase}.OTUs.list, count=${inBase}.count_table, taxonomy=${taxonomyFile}, label=$clusteringPercentIdentLabel, probs=f, persample=t, basis=sequence, group=${groupsFile})"
    ${mothur} "#set.logfile(name=${mothurLogs}/mothur.classify.OTUs.log);classify.otu(list=${inBase}.OTUs.list, count=${inBase}.count_table, taxonomy=${taxonomyFile}, label="0.03", probs=f, persample=t, basis=sequence)"

    taxHeader=""
    tail -qn +2 ${inBase}.OTUs.0.03.cons.taxonomy | sort -k 1b,1 > ${inBase}.OTUs.0.03.cons.taxTableOTU
    touch ${inBase}.OTUs.0.03.cons.tax_intermediate
    for group in `echo $allGroups | tr "-" " "`; do
		tail -qn +2 ${inBase}.OTUs.0.03.${group}.cons.taxonomy | sort -k 1b,1 | join -a1 -a2 -o 2.2 -e "0" ${inBase}.OTUs.0.03.cons.taxTableOTU - | paste ${inBase}.OTUs.0.03.cons.tax_intermediate - > ${inBase}.OTUs.0.03.cons.tax_intermediate2
		mv ${inBase}.OTUs.0.03.cons.tax_intermediate2 ${inBase}.OTUs.0.03.cons.tax_intermediate
		taxHeader="${taxHeader}\t${group}"
    done
    echo -e "OTU\tTaxonomy\tTotalSize${taxHeader}" > ${inBase}.OTUs.0.03.cons.taxTableOTU_intermediate
    $cat ${inBase}.OTUs.0.03.cons.taxTableOTU | awk '{print $1,"\t",$3,"\t",$2}' | paste - ${inBase}.OTUs.0.03.cons.tax_intermediate | sort -nr -k 3,3 >> ${inBase}.OTUs.0.03.cons.taxTableOTU_intermediate
    mv ${inBase}.OTUs.0.03.cons.taxTableOTU_intermediate ${inBase}.OTUs.0.03.cons.taxTableOTU
    rm ${inBase}.OTUs.0.03.cons.tax_intermediate	

    for level in {1..6}; do
		echo -e "Taxa\tTotalSize${taxHeader}" > ${inBase}.OTUs.0.03_L${level}.taxTableTotal
		$grep "^$level" ${inBase}.OTUs.0.03.cons.tax.summary | $grep -v unclassified | $awk '{all=$3;for(i=5;i<=NF;++i)all=all"\t"$i;print all}' | sort -k 1 >> ${inBase}.OTUs.0.03_L${level}.taxTableTotal
		$grep "^$level" ${inBase}.OTUs.0.03.cons.tax.summary | $grep unclassified | $awk '{all="";for(i=5;i<=NF;++i)all=all"\t"$i;print all}'| $awk '{for(i=1;i<=NF;++i)sum[i]+=$i}END{line="";for(i=1;i<=NF;++i)line=line"\t"sum[i];print "unclassified"line}' >> ${inBase}.OTUs.0.03_L${level}.taxTableTotal
    done

    #diversity analysis
    echo -e "\n"
    echo -e "[MOTHUR] ** calculate diversity metrics **"
    ${mothur} "#set.logfile(name=${mothurLogs}/mothur.list.seqs.log);list.seqs(list=${inBase}.OTUs.list)"
    ${mothur} "#set.logfile(name=${mothurLogs}/mothur.get.seqs.log);get.seqs(accnos=${inBase}.OTUs.accnos, count=${inBase}.count_table)"
    ${mothur} "#set.logfile(name=${mothurLogs}/mothur.make.shared.log);make.shared(list=${inBase}.OTUs.list, count=${inBase}.pick.count_table)"

    ${mothur} "#set.logfile(name=${mothurLogs}/mothur.collect.shared.log);collect.shared(shared=${inBase}.OTUs.shared, freq=0.025)"
    ${mothur} "#set.logfile(name=${mothurLogs}/mothur.rarefaction.log);rarefaction.shared(shared=${inBase}.OTUs.shared)"
    ${mothur} "#set.logfile(name=${mothurLogs}/mothur.summary.shared.log);summary.shared(shared=${inBase}.OTUs.shared, processors=$numProc, all=f)"
    ${mothur} "#set.logfile(name=${mothurLogs}/mothur.dist.shared.log);dist.shared(shared=${inBase}.OTUs.shared, processors=$numProc)"
    ${mothur} "#set.logfile(name=${mothurLogs}/mothur.heatmap.bin.log);heatmap.bin(shared=${inBase}.OTUs.shared, sorted=topotu)"
    ${mothur} "#set.logfile(name=${mothurLogs}/mothur.heatmap.sim.log);heatmap.sim(shared=${inBase}.OTUs.shared)"
    #${mothur} "#set.logfile(name=${mothurLogs}/mothur.venn.log);venn(shared=${inBase}.OTUs.shared)"
    ${mothur} "#set.logfile(name=${mothurLogs}/mothur.tree.shared.log);tree.shared(shared=${inBase}.OTUs.shared)"

    #cleaning up Mothur and preparing TUIT and error analysis 
    echo -e "\n"



 
    echo -e "\n\n\n [FINISHED] finished complete amplicon pipe. All Results are written to: ${outDir}/results"

    exit 0;
