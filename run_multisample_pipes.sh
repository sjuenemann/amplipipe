#! /bin/bash

function usage {
    echo -e "\n  THIS script starts one of the amplicon MiSeq EASY pipelines while iterating over an input lists which are giving the input data pairs and the used primers"
    echo -e "\n  Usage: $0 -i <input_list> -p <path_to_single_sample_results> -s <path_to_script> -o <output_dir>" 
    echo -e "\n"
    echo -e "\t-i  : the input list file that was used for the single sample analysis"
    echo -e "\t-p  : the directory where the single sample results are sored (base outdir)"
    echo -e "\t-s  : the script to be used for the multisample analysis (/vol/biotools/share/AmpliconPipe/ampliconpipe_multisample.sh)"
    echo -e "\t-o  : the output directory"
}

while getopts 'i:p:s:o:P:h' opt
do
   case $opt in
    i) input=$OPTARG;;
    p) singleDir=$OPTARG;;
    s) script=$OPTARG;;
    o) outDir=$OPTARG;;
	P) params=$OPTARG;;
    h) usage;;
    \?)
        echo "Invalid option: -$OPTARG" >&2
        exit 1
        ;;
    :)
        echo "Option -$OPTARG requires an argument." >&2
        exit 1
        ;;
    esac
done

if [  "a${script}" == "a" -o ! -r "${script}" ]; then
    echo -e " Cannot read the script file: $script. Aborting!"
    usage;
    exit 1;
fi

input=`readlink -f ${input}`
singleDir=`readlink -f ${singleDir}`
if [ "a${outDir}" != "a" ]; then
    if [ ! -d ${outDir} ]; then
        echo -e "[OUT] No output directory given. Creating output directory: ${outDir}"
    mkdir ${outDir}
    else
        echo -e "[OUT] Output directory given. Using ${outDir}"
    fi
    echo -e "[OUT] Switching to output_dir: ${outDir}."
    cd ${outDir}
    outDir=`pwd`
fi
outBase=`basename $input`
outBase=$PWD/${outBase%%.*}


counter=1
while read inLine; do
    inA=`echo $inLine | awk '{print $1}'`
    inB=`echo $inLine | awk '{print $2}'`
    if [  "a${inA}" == "a" -o ! -r "${inA}" ]; then
		echo -e " Cannot read first input data: $inA. Aborting!";
		exit 1;
    fi
    if [  "a${inB}" == "a" -o ! -r "${inB}" ]; then
		echo -e " Cannot read second input data: $inB. Aborting!";
		exit 1;
    fi
    echo -e "[$counter] Processing the $counter line of $input"
    # get the pre-processed fasta file for the multi-sample analysis
    base1=`basename $inA`
    base2=`basename $inB`
    outbase=${base1%.*}_${base2%.*}
    outdir=`readlink -f ${singleDir}/${base1%.*}_${base2%.*}`
    echo -e "[$counter] Searching for single-run output in $outdir ..."
    if [ ! -d ${outdir} ]; then
		echo -e "Something went wrong in the pipeline $script. Output dir $outdir was not created but expected. Aborting!"
		exit 1;
    fi
    #HQFasta=${outdir}/mothur_pipeline/merged.highQual.unique.good.filter.unique.precluster.pick.fasta
    #HQCount=${outdir}/mothur_pipeline/merged.highQual.unique.good.filter.unique.precluster.pick.count_table
    #HQTax=${outdir}/mothur_pipeline/merged.highQual.unique.good.filter.unique.precluster.pick.nr_v119.wang.taxonomy
    HQFasta=${outdir}/mothur_pipeline/*.highQual.unique.good.filter.unique.precluster.pick.unique.fasta
    HQCount=${outdir}/mothur_pipeline/*.highQual.unique.good.filter.unique.precluster.pick.count_table
    HQTax=${outdir}/mothur_pipeline/*.highQual.unique.good.filter.unique.precluster.pick.*.wang.taxonomy
    if [ ! -e $HQFasta -a ! -e $HQCount -a ! -e $HQTax ]; then
		echo -e "Something went wrong in the single pipeline for $outdir. Output dir was not completed. Aborting!"
		exit 1;
    fi
    ln -s $HQFasta $outDir/${outbase//-/_}.fasta
    ln -s $HQCount $outDir/${outbase//-/_}.count_table
    ln -s $HQTax $outDir/${outbase//-/_}.taxonomy
    echo -e "[$counter] ... done."
    counter=$(($counter+1))
done < $input

# now perform the multi-sample analysis
echo -e " Performing multi-sample analysis: $script -i $outDir -o $outBase $params 2> ${outBase}.err > ${outBase}.log"
$script -i $outDir -o $outBase $params 2> ${outBase}.err > ${outBase}.log
cd ..
echo -e " ... done!"



