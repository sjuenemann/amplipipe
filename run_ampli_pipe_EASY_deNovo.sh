#!/bin/bash

# this is a wrapper script for running the amplicon pipeline
# it takes 3 arguments: forward.fastq, reverse.fastq and a primer.txt file
# it then runs the pipeline with all pre-defined paramsters and databases
# output is stored within a directory named based on the input data

if [ "$1" == "-h" -o "$1" == "" -o $# \< 3 ]; then
  echo "
    Usage: `basename $0` <forward.fastq> <reverse.fastq> <primers.txt>

    Wrapper script for ampliconpipe_miseq_pe_merge.sh.
    It predefines several paramters and then calls the ampliconpipe.sh with the input and primer file passed to this wrapper script.
	Additional parameters to be passed to the script (e.g. "-k -q 13" for qc filtering before PE read assembly) can be passed as fourth parameter within single quotes
    "
  exit 0
fi

baseDir=$(dirname `readlink -f $0`)
alignmentDB=${baseDir}/databases/silva/silva.seed.align
classDB=${baseDir}/databases/silva/silva.full.fasta
taxDB=${baseDir}/databases/silva/silva.full.tax

p1=$1
shift
p2=$1
shift
p3=$1
shift

base1=`basename $p1`
base2=`basename $p2`

outdir=${base1%.*}_${base2%.*}

echo "Executing: ${baseDir}/ampliconpipe_miseq_pe_merge.sh -f $p1 -s $p2 -p $p3 -o $outdir -r $alignmentDB -l $classDB -t $taxDB  $@ >> ${outdir}.log 2>> ${outdir}.err" >>${outdir}.log
${baseDir}/ampliconpipe_miseq_pe_merge.sh -f $p1 -s $p2 -p $p3 -o $outdir -r $alignmentDB -l $classDB -t $taxDB  $@ >> ${outdir}.log 2>> ${outdir}.err


