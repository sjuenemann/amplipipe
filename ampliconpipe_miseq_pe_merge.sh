#! /bin/bash

# This script executes takes MiSeq paired-end data (seperate, interleaved, merged) and performs several 16S rRNA analysis steps (utilizing mothur/USEARCH) on them

#1 USAGE
function usage {
    #echo -e "\n  Usage: $0 -f <forward_reads.fastq> -s <reverse_reads.fastq> | -i <interleaved.fastq> | -m <merged.fastq> -r </path/to/reference_database> -c </path/to/reference_database> -g </path/to/mock.fasta> -o <output_dir> "
    echo -e "\n  THIS script starts the amplicon MiSeq pipeline which in essence performs the analysis steps as described here: [TODO]"
    echo -e "\n  Usage: $0 -f <forward_reads.fastq> -s <reverse_reads.fastq> | -m <merged.fastq> -r </path/to/reference_alignment_database> -c </path/to/reference_chimerafree_database> 
		           -l </path/to/reference_class_fasta_database> -t </path/to/reference_class_tax_database> -u <enable_uchime_ref> -g </path/to/mock.fasta> -p <primers.txt> -T <enables TUIT classification> -o <output_dir>"
    echo -e "\t==One of the following input methods have to be used=="
    echo -e "\t-f  path to the forward reads file (FASTQ format)"
    echo -e "\t-s  path to the reverse reads file (FASTQ format)"
   # echo -e "==or=="
   # echo -e "-i  path to forward and reverse reads fastq (FASTQ interleaved format; single file)"
    echo -e "\t==or=="
    echo -e "\t-m  path to merged forward and reverse reads (FASTQ format; single file; skipping merging step)"
    echo -e "\t==Aligned and classification database is mandatroy. If chimera DB is omitted and uchime_ref should be used, the alignment ref BD will be used for chimera detection=="
    echo -e "\t-r  path to the reference database to be used for aligning reads (needs to be aligned)"
    echo -e "\t-c  path to the reference database to be used for chimera detection (only recommended if using a MOCK sample)"
    echo -e "\t-l  path to the reference database to be used for classification (needs to be a fasta file)"
    echo -e "\t-t  path to the reference database to be used for classification (needs to be a tax file)"
    echo -e "\t==Optional arguments=="
    echo -e "\t-g  path to a FASTA file containing the MOCK sample references (if available). If given and merging should be done, than fragment size will be calculated"
    echo -e "\t    based on a mapping of the reads to this reference. Otherwise, merger will assume this from PE-assembly progess."
    echo -e "\t-p  A file listing all the primers which should be searched for and removed in two columns (first primer type, second the sequence), e.g.: "
    echo -e "\t    forward\tATGC"
    echo -e "\t    reverse\tATGC"
    echo -e "\t    Multiple primers can be given by repeating entries. IUPAC code is supported (for wobble bases). If omitted, no primers will be searched and removed."
    echo -e "\t-o  path to the output directory where all results and intermediate files are stored. If omitted, "." will be used. If not existend, it will be created."
    echo -e "\t-u  use UCHIME reference based chimera detection. This requeire a MOCK file to be given."
    echo -e "\t-T  enables TUIT classification (uses the ${referenceTUIT} database). Takes pretty long to execute."
    echo -e "\t-H  stops the execution after generating high quality data."
    echo -e "\t-S  skips the length trimming at 1,5IQR."
    echo -e "\t-k  performs quality clipping beforer merging reads."
    echo -e "\t-q  threshold for quality clipping before read merging."
    echo -e "\t-d  removes the alignment file alignment based filtering has been performed."
    exit 1
}

useTUIT=0;
useUCHIMEREF=0
stopHQ=0
clipBeforeMerge=0
cleanUP=0
skipTrim=0
configFile=$(dirname `readlink -f $0`)/build_environment

while getopts 'f:s:i:m:r:c:l:t:o:g:p:q:C:kuhTHdS' opt
do
   case $opt in
    f) inputF=$OPTARG;;
    s) inputR=$OPTARG;;
    i) inputI=$OPTARG;;
    m) inputM=$OPTARG;;
    r) referenceA=$OPTARG;;
    c) referenceC=$OPTARG;;
    l) referenceClassFas=$OPTARG;;
    t) referenceClassTax=$OPTARG;;
    o) outDir=$OPTARG;;
    g) MOCK=$OPTARG;;
    p) PRIMERS=$OPTARG;;
    u) useUCHIMEREF=1;;
    T) useTUIT=1;;
    H) stopHQ=1;;
    k) clipBeforeMerge=1;;
    q) cipQualityPE=$OPTARG;;
	d) cleanUP=1;;
    S) skipTrim=1;;
	C) configFile=$OPTARG;;
    h) usage;;
    :)
        echo "Option -$OPTARG requires an argument." >&2
        exit 1
        ;;
    \?)
        echo "Invalid option: -$OPTARG" >&2
        exit 1
        ;;
    esac
done


## providing info about passed arguments
echo -e "[CLI] Started amplicon miseq pipeline with the following parameters:
    f= $inputF
    s= $inputR
    i= $inputI
    m= $inputM
    r= $referenceA
    c= $referenceC
    l= $referenceClassFas
    t= $referenceClassTax
    o= $outDir
    g= $MOCK
    p= $PRIMERS
    u= $useUCHIMEREF
    T= $useTUIT
    H= $stopHQ
	S= $skipTrim
    k= $clipBeforeMerge
    q= $cipQualityPE
	d= $cleanUP
	C= $configFile
		"	

source $configFile

# check if we are running on a cluster host, and if so, print some info about executing host and job
if [ "$SGE_O_HOST" != "" ]; then
	$scriptDir/checkLocalHostStats
fi

#### Sanity checks on parameters #####
## 1: Input
if [  "a${inputF}" == "a" -a "a${inputR}" != "a" ]; then
    echo -e "[CLI] No forward input given. Aborting!"
    usage;
fi
if [  "a${inputF}" != "a" -a "a${inputR}" == "a" ]; then
    echo -e "[CLI] No reverse input given. Aborting!"
    usage;
fi
if [  "a${inputF}" == "a" -a "a${inputR}" == "a" -a "a${inputI}" == "a" -a "a${inputM}" == "a" ]; then
    #echo -e "[CLI] No input given. Provide either forward and reverse reads, a interleaved file, or a merged file in FASTQ format. Aborting!"
    echo -e "[CLI] No input given. Provide either forward and reverse reads or a merged file in FASTQ format. Aborting!"
    usage;
fi
if [  "a${inputF}" == "a" -a "a${inputR}" == "a" -a "a${inputI}" != "a" -a "a${inputM}" != "a" ]; then
    echo -e "[CLI] Interleaved and merged input given. Provide either one of the following: forward and reverse reads, a interleaved file, _or_ a merged file in FASTQ format. Aborting!"
    usage;
fi
if [  "a${inputF}" != "a" -a "a${inputR}" != "a" -a "a${inputI}" != "a" -a "a${inputM}" != "a" ]; then
    #echo -e "[CLI] All input types. Provide either one of the following: forward and reverse reads, a interleaved file, _or_ a merged file in FASTQ format. Aborting!"
    echo -e "[CLI] All input types. Provide either one of the following: forward and reverse reads  _or_ a merged file in FASTQ format. Aborting!"
    usage;
fi
if [  "a${inputF}" != "a" -a "${inputF}" == "${inputB}" ]; then
    echo -e "[CLI] Forward and reverse reads are equal. Two (separate) input files have to be given. Aborting!"
    usage;
fi
if [  "a${inputF}" != "a" -a ! -r "${inputF}" ]; then
    echo -e "[CLI] Cannot read seqeuncing file: ${inputF}.Aborting!"
    exit 1
else
    inputF=`realpath ${inputF}`
fi
if [ "a${inputR}" != "a" -a ! -r "${inputR}" ]; then
    echo -e "[CLI] Cannot read seqeuncing file: ${inputR}.Aborting!"
    exit 1
else
    inputR=`realpath ${inputR}`
fi
if [ "a${inputI}" != "a" -a ! -r "${inputI}" ]; then
    echo -e "[CLI] Cannot read seqeuncing file: ${inputI}.Aborting!"
    exit 1
fi
if [ "a${inputM}" != "a" -a ! -r "${inputM}" ]; then
    echo -e "[CLI] Cannot read seqeuncing file: ${inputM}.Aborting!"
    exit 1
fi
if [ "a${inputM}" != "a" ]; then
    isMerged="1"
    inputM=`realpath ${inputM}`
fi
if [ "a${inputI}" != "a" ]; then
    isInterleaved="1"
    inputI=`realpath ${inputI}`
    echo -e "Interleaved (hidden option) format for first level input is currently not supported. Either provide two seperate files or a already merged one. Aborting!"
    usage;
    exit 1;
fi
##2: References
if [ "a${referenceA}" == "a" -a "a${referenceC}" == "a" ]; then
    echo -e "[CLI] No alignment reference database given. Provide at least an alignment DB. Aborting!"
    usage;
    exit 1;
fi
if [ "a${referenceC}" == "a" -a "a${referenceA}" != "a" -a "${useUCHIMEREF}" == "1" ]; then
    echo -e "[CLI] Alignment database given: Setting chimera detection database for reference based chimera detection to: ${referenceA}."
    echo -e "[CLI] Warning. Depending on the alignment DB, this may result in long and unreliable chimera detection. For uchime small databases are recommendend (e.g. the RDP train set)."
    referenceC=${referenceA}
fi
if [ "a${referenceA}" == "a" -a "a${referenceC}" != "a" ]; then
    echo -e "[CLI] Chimera detection database given: Setting alignment database to: ${referenceC}."
    echo -e "[CLI] Warning. Depending on the chimera detection DB, this may result in a failed alignment as mothur needs a DB in aligned format."
    referenceA=${referenceC}
fi
if [ "a${referenceA}" != "a" -a ! -r "${referenceA}" ]; then
    echo -e "[CLI] Cannot read database file: ${referenceA}.Aborting!"
    exit 1;
else
    referenceA=`realpath ${referenceA}`
fi

if [ "a${referenceClassFas}" == "a" -a "a${referenceClassFas}" == "a" ]; then
    echo -e "[CLI] No classification FASTA DB given. Provide a FASTA DB used for read taxonomic classification. Aborting!"
    usage;
    exit 1;
fi
if [ "a${referenceClassTax}" == "a" -a "a${referenceClassTax}" == "a" ]; then
    echo -e "[CLI] No classification Tax DB given. Provide a Tax DB used for read taxonomic classification. Aborting!"
    usage;
    exit 1;
fi

if [ "${useUCHIMEREF}" == 1 -a "a${referenceC}" != "a" -a ! -r "${referenceC}" ]; then
    echo -e "[CLI] Cannot read database file: ${referenceC}.Aborting!"
    exit 1;
fi
if [ "${useUCHIMEREF}" == 1 ]; then 
    referenceC=`realpath ${referenceC}`
fi

#if [ "a${MOCK}" == "a" -a "$useUCHIMEREF" == 1 ]; then
#    echo -e "[CLI] UCHIME reference based chimera detection can only be used if a MOCK file is given.Aborting!"
#    usage;
#    exit 1;
#fi

if [ "a${MOCK}" != "a" -a ! -r "${MOCK}" ]; then
    echo -e "[CLI] MOCK fasta file given but cannot be accessed: ${MOCK}.Aborting!"
    exit 1;
fi
if [ "a${MOCK}" != "a" -a -r "${MOCK}" ]; then
    MOCK=`realpath ${MOCK}`
fi

if [ "a${PRIMERS}" != "a" -a ! -r "${PRIMERS}" ]; then
    echo -e "[CLI] Cannot read given primer file: ${PRIMERS}.Aborting!"
    exit 1;
else
	if [ "a${PRIMERS}" != "a" ]; then
    	PRIMERS=`realpath ${PRIMERS}`
    	primersGiven="1"
	fi
fi

if [ "a${referenceClassFas}" != "a" -a ! -r "${referenceClassFas}" ]; then
    echo -e "[CLI] Cannot read database file: ${referenceClassFas}.Aborting!"
    exit 1;
else
    referenceClassFas=`realpath ${referenceClassFas}`
fi
if [ "a${referenceClassTax}" != "a" -a ! -r "${referenceClassTax}" ]; then
    echo -e "[CLI] Cannot read database file: ${referenceClassTax}.Aborting!"
    exit 1;
else
    referenceClassTax=`realpath ${referenceClassTax}`
fi


####create outdir if given####
if [ "a${outDir}" != "a" ]; then
    if [ ! -d ${outDir} ]; then
    	echo -e "[OUT] Output directory given. Creating output directory: ${outDir}"
	mkdir ${outDir}
    else 
    	echo -e "[OUT] Output directory given. Using ${outDir}"
    fi
    echo -e "[OUT] Switching to output_dir: ${outDir}."
    cd ${outDir}
    outDir=`pwd`
else
    echo -e "[OUT] Output directory not given. Using current directory: ${PWD}"
    outDir=${PWD}
fi
#############################


####merge pairs####
if [ "$isMerged" != "1" ]; then
    echo -e "\n\n"
    echo -e "[MERGE] Inferring read length from input ..."
    if [ "$isInterleaved" == "1" ];   then
        $perl ${scriptDir}/FastaStats.pl -q ${inputI} > ${inputI}.hist
        readLen=`$grep "Mean length" ${inputI}.hist | awk '{print $3}'`
    else
        inputFHist=`basename ${inputF}`.hist
        inputRHist=`basename ${inputR}`.hist
    	echo -e "[MERGE] Calling $perl ${scriptDir}/FastaStats.pl -q ${inputF} > ${inputFHist}"
    	echo -e "[MERGE] Calling $perl ${scriptDir}/FastaStats.pl -q ${inputR} > ${inputRHist}"
        $perl ${scriptDir}/FastaStats.pl -q ${inputF} > ${inputFHist}
        $perl ${scriptDir}/FastaStats.pl -q ${inputR} > ${inputRHist}
        readLen=`$grep "Mean length" ${inputFHist} | awk '{print $3}'`
    fi
    readLen=`printf '%0.f' $readLen`
    if [ -r "${MOCK}" ]; then
		echo -e "[MERGE] ... done. Average read length is: $readLen."
		echo -e "[MERGE] MOCK file given, trying to infer fragment size and standard deviation ..."
		if [ "$isInterleaved" == "1" ];   then
	    	estimateParam=""
		else
	    	estimateParam="${inputF} ${inputR} ${MOCK}"
		fi
		echo -e "[MERGE] Executing: ${scriptDir}/ngscpipe_estimatePEinsert ${estimateParam} '"
		insertHist=`${scriptDir}/ngscpipe_estimatePEinsert ${estimateParam} | $grep -o -E "Insertsize results were written to (.*_insertHist)" | awk '{print $6}'`
		if [ "a$insertHist" == "a" ]; then
		    echo -e "[MERGE] Something went wrong, merging failed. Aborting! ${insertHist}"
	    	exit 1;
		fi
		fragSize=`$cat ${insertHist}.txt | $grep -A 2 "Summary of outlier removed values is:" | tail -n 1 | awk '{print $4}' | xargs printf "%.0f"`
		fragStdv=`$cat ${insertHist}.txt | $grep -A 2 "SD is:"| tail -n 1 | awk '{print $2}'| xargs printf "%.0f"` 
		echo -e "[MERGE] ...done. Fragment size is: $fragSize; standward deviation is: $fragStdv";
    else
		echo -e "[MERGE] Estimating Fragment size from read length"
		fragSize=`perl -e "printf \"%.f\", ($readLen * 2) - (($readLen * 2 * 10) / 100)"`
		fragStdv=`perl -e "printf \"%.f\", ($fragSize/10)"`	
    fi

	##### Perform quality clipping before merging####
	flashParam="-f ${fragSize} -s ${fragStdv}"
	if [ "$clipBeforeMerge" == "1" ]; then
	    echo -e "[MERGE] Calling Iteratve FLASH: ${baseDir}/iterative_flash.sh -f ${inputF} -r ${inputR} -l $readLen -s $fragSize -S $fragStdv -q 3" 
		${baseDir}/iterative_flash.sh -f ${inputF} -r ${inputR} -l $readLen -s $fragSize -S $fragStdv -q 3
	else 
    	if [ "$isInterleaved" == "1" ];   then
			inputI=`realpath ${inputI}`
			flashParam="-I ${inputI} ${flashParam}"
    	else
			inputF=`realpath ${inputF}`
			inputR=`realpath ${inputR}`
			flashParam="${inputF} ${inputR} ${flashParam}"
	    fi
    	echo -e "[MERGE] Merging forward and reverse reads ..."
	    echo -e "[MERGE] executing: ${binDir}flash ${flashParam} -o merged -r ${readLen}"
    	${binDir}flash ${flashParam} -o merged -r ${readLen} 
	fi
    ln -s merged.extendedFrags.fastq merged.fastq
    inputM="merged.fastq"
    echo -e "[MERGE] ... done."
    outPrefix=`basename ${inputF%%.fastq}`_`basename ${inputR%%.fastq}`
else
    outPrefix=`basename ${inputM}`
fi
inputM=`realpath -s ${inputM}`
#############################


####remove primers####
if [ "${primersGiven}" == "1" ]; then
    echo -e "\n\n"

    echo -e "[PRIMER] Primers given. Trying to remove given primers."
    echo -e "[PRIMER] Reading primer file ..."

    forwardP=`$grep "forward" ${PRIMERS} | awk '{print $2}' | tr "\n" " "`
    reverseP=`$grep "reverse" ${PRIMERS} | awk '{print $2}' | tr "\n" " "`
    forwardPR=`echo ${forwardP} | ${scriptDir}/RevCompSeq.pl`
    reversePR=`echo ${reverseP} | ${scriptDir}/RevCompSeq.pl`

    echo -e "[PRIMER] ... done. Primers are: \n \t \t forward: ${forwardP} \n \t \t reverse: ${reverseP}"
    
    primerAllowedError="0.2"	
    primerOverlap="10"	
    primerParams=""

    echo -e "[PRIMER] Creating primer removal subdir: ${outDir}/primer_removal"
    mkdir ${outDir}/primer_removal
    echo -e "[PRIMER] Switching to primer removal subdir: ${outDir}/primer_removal"
    cd ${outDir}/primer_removal
    ln -s ${inputM}
    
    echo -e "[PRIMER] Executing Primer screening and removal ..."
    #forward primer
    cutadapt `printf -- "-g ^%s " ${forwardP[*]}` ${inputM} --untrimmed-output untrimmed_f.fastq -o trimmed_f.fastq -r f.rest -e $primerAllowedError -O $primerOverlap $primerParams
    cutadapt `printf -- "-g ^%s " ${reverseP[*]}` untrimmed_f.fastq --untrimmed-output rev_untrimmed -o to_flip.fastq  -r rev_mid.rest -e $primerAllowedError -O $primerOverlap $primerParams
    if [ -z trimmed_f.fastq -o -z to_flip.fastq ]; then
	echo -e "[PRIMER] something went wrong. Neither the forward primer nor the reverse complement of the reverse primer could be found/trimmed in the input data. Please check your primers. Aborting!"
	exit 1;
    fi
    #reverse primer
    if [ -s trimmed_f.fastq ]; then
        cutadapt `printf -- "-a %s$ " ${reversePR[*]}` trimmed_f.fastq --untrimmed-output untrimmed_r.fastq -o trimmed_fr.fastq -r r.rest -e $primerAllowedError -O $primerOverlap $primerParams
    fi
    if [ -s to_flip.fastq ]; then
	cutadapt `printf -- "-a %s$ " ${forwardPR[*]}` to_flip.fastq --untrimmed-output to_flip_untrimmed.fastq -o trimmed_to_flip.fastq -r to_flip.rest -e $primerAllowedError -O $primerOverlap $primerParams
    fi
    echo -e "[PRIMER] ... done."

    if [ -s trimmed_to_flip.fastq ]; then
	echo -e "[PRIMER] Creating reverse complement of reads matched the revcomp primers ..."
	echo -e "[PRIMER] Executing: ${binDir}fastx_reverse_complement -i trimmed_to_flip.fastq -o flipped_trimmed.fastq -Q33"
	${binDir}fastx_reverse_complement -i trimmed_to_flip.fastq -o flipped_trimmed.fastq -Q33
	$cat flipped_trimmed.fastq trimmed_fr.fastq > trimmed_primers.fastq
	echo -e "[PRIMER] ... done."
    else 
	ln -s trimmed_fr.fastq trimmed_primers.fastq
    fi
    cd ..
    ln -s ${outDir}/primer_removal/trimmed_primers.fastq ${inputM%%.fastq}.trimmed.fastq
    echo -e "[PRIMER] Primer removal completed. File is: ${inputM%%.fastq}.trimmed.fastq"
    currentIn=${inputM%%.fastq}.trimmed.fastq
else
    currentIn=${inputM%%.fastq}.fastq
fi    
#############################


##### Perform quality clipping####
    echo -e "\n\n"
    echo -e "[CLIP] Perform quality clipping"
    echo -e "[CLIP] Clip to average quality level ..."
    echo -e "[CLIP] Executing: ${binDir}sickle se -f $currentIn -t sanger -o ${currentIn%%.fastq}.clip.fastq -q ${clipQuality} -n"
    ${binDir}sickle se -f $currentIn -t sanger -o ${currentIn%%.fastq}.clipped.fastq -q ${clipQuality} -n
    echo -e "[CLIP ... done."
    currentIn=${currentIn%%.fastq}.clipped.fastq

  	if [ "${skipTrim}" == "0" ]; then
    # compute length histogram and filter read length on Q1/Q3 +/- 1.5*IQR
        echo -e "[CLIP] Compute Length Histogram and filter read length on Q1/Q3 +/- 1.5*IQR ..."
        $perl ${scriptDir}/FastaStats.pl -q ${currentIn} > ${currentIn}.hist
        low=`grep -E "^Q1" ${currentIn}.hist`
        low=${low##*low=}
        high=${low##*high=}
        low=${low%% high=*}
        echo -e "[CLIP] Low and high boundaries are: low=$low high=$high"
        echo -e "[CLIP] Executing: perl ${scriptDir}/FilterReadsOnLength.pl ${currentIn} $low $high > ${inputM%%.fastq}.highQual.fasta 2> ${currentIn%%.fasta}.to_short_long.fasta"
        $perl ${scriptDir}/FilterReadsOnLength.pl ${currentIn} $low $high > ${inputM%%.fastq}.highQual.fasta 2> ${currentIn%%.fasta}.to_short_long.fasta
        #echo -e "[CLIP] Executing: ${binDir}fastq-mcf -0 -l $low -L $high n/a ${currentIn} > ${inputM%%.fastq}.highQual.fastq 2> ${currentIn%%.fastq}.to_short_long.fastq"
        #${binDir}fastq-mcf -0 -l $low -L $high n/a ${currentIn} -o ${inputM%%.fastq}.highQual.fastq 2> ${currentIn%%.fastq}.to_short_long.log
        echo -e "[CLIP] ... done. High quality reads are: ${inputM%%.fastq}.highQual.fastq"
	else
		mv ${currentIn} ${inputM%%.fastq}.highQual.fastq
        echo -e "[CLIP] Skipping length trimming."
    fi
    currentIn=${inputM%%.fastq}.highQual.fastq
    # convert fastq to fasta
    echo -e "[CLIP] Convert to FASTA"
    ${binDir}fastq_to_fasta -i $currentIn -o ${currentIn%%.fastq}.fasta -Q33
###########################

if [ "${stopHQ}" == "1" ]; then
	mv ${currentIn} ../${outPrefix%%_R1*}.fastq
	echo -e "Stopping after generated HQ data: ${outPrefix}.fasta. Done"
	exit 0;
else 
	currentIn=${inputM%%.fastq}.highQual.fasta
	echo -e "[CLIP] High quality FATSA reads generated used for mothur: $currentIn."
fi
####Now switch using mothur for the real amplicon pipeline####
    echo -e "\n\n"
    echo -e "[MOTHUR] Statring amplicon pipelien parts"
    echo -e "[MOTHUR] Creating mothur output folder: ${outDir}/mothur_pipeline and switching to it"
    mkdir ${outDir}/mothur_pipeline
    cd ${outDir}/mothur_pipeline
    mkdir mothur_logs
    ln -s ${currentIn}
    inBase=`basename ${currentIn}`
    inBase=${inBase%%.fasta}

    # de-replicate sequences
    echo -e "\n"
    echo -e "[MOTHUR] ** filter unique sequences **"
    ${mothur} "#set.logfile(name=mothur_logs/mothur.unique.log);unique.seqs(fasta=${inBase}.fasta)" 
    echo -e "\n"
    echo -e "[MOTHUR] ** create count file **"
    ${mothur} "#set.logfile(name=mothur_logs/mothur.count.log);count.seqs(name=${inBase}.names)"

    #align sequences to refereence, filter all reads which align to different reference position and collapse the alignment
    echo -e "\n"
    echo -e "[MOTHUR] ** align sequences to reference DB: ${referenceA} **"
    ${mothur} "#set.logfile(name=mothur_logs/mothur.align.log);align.seqs(fasta=${inBase}.unique.fasta, flip=t, reference=${referenceA}, processors=$numProc)"
    ${mothur} "#set.logfile(name=mothur_logs/mothur.summary.align.log);summary.seqs(fasta=${inBase}.unique.align, count=${inBase}.count_table, processors=$numProc)"
    minStart=`grep -E "^2.5" mothur_logs/mothur.summary.align.log | awk '{print $2}'`
    minEnd=`grep -E "^97.5" mothur_logs/mothur.summary.align.log | awk '{print $3}'`

    echo -e "\n"
    echo -e "[MOTHUR] ** screen for alignemnts out of target range and filter for minStart=${minStart} and minEnd=${minEnd} **"
    ${mothur} "#set.logfile(name=mothur_logs/mothur.screen.align.log);screen.seqs(fasta=${inBase}.unique.align, count=${inBase}.count_table, summary=${inBase}.unique.summary, start=$minStart, end=$minEnd, processors=$numProc)"
	# if that removed to many sequences we use the 25% and 75% tile for alingment position filtering
	numberOfReads=$((`wc -l merged.highQual.names | awk '{print $1}'`/2))
	numberOfBadReads=`wc -l merged.highQual.unique.bad.accnos | awk '{print $1}'`
	if [ $numberOfBadReads -gt $numberOfReads ]; then
    	echo -e "[MOTHUR] ** removed to many reads using minStart=${minStart} and minEnd=${minEnd} ($numberOfBadReads removed). Switching to 25% and 75% tile **"
    	minStart=`grep -E "^25" mothur_logs/mothur.summary.align.log | awk '{print $2}'`
	    minEnd=`grep -E "^75" mothur_logs/mothur.summary.align.log | awk '{print $3}'`
    	echo -e "[MOTHUR] ** screen again for alignemnts out of target range and filter for minStart=${minStart} and minEnd=${minEnd} **"
    	${mothur} "#set.logfile(name=mothur_logs/mothur.screen.align.log);screen.seqs(fasta=${inBase}.unique.align, count=${inBase}.count_table, summary=${inBase}.unique.summary, start=$minStart, end=$minEnd, processors=$numProc)"
	fi

    #### TODO
    # for any strange reason, there is a bug in the latest 1.35.1 version of mothur that duobles the last line of the count_table after using screen.seqs
    #head -n -1 ${inBase}.good.count_table > ${inBase}.good.count_table.cleaned
    #mv ${inBase}.good.count_table.cleaned ${inBase}.good.count_table
    ${mothur} "#set.logfile(name=mothur_logs/mothur.filter.align.log);filter.seqs(fasta=${inBase}.unique.good.align, vertical=T, trump=., processors=$numProc)"
    ${mothur} "#set.logfile(name=mothur_logs/mothur.unique.align.log);unique.seqs(fasta=${inBase}.unique.good.filter.fasta, count=${inBase}.good.count_table)"

	if [ "$cleanUP" == 1 ]; then
		echo -e "[CLEANUP] removing intermedtiate alignment data"
		rm ${inBase}.unique.good.align
		rm ${inBase}.unique.align
	fi

    # pre-cluster seqeunces 
    echo -e "\n"
    echo -e "[MOTHUR] ** pre-cluster sequences  **"
    ${mothur} "#set.logfile(name=mothur_logs/mothur.precluster.log);pre.cluster(fasta=${inBase}.unique.good.filter.unique.fasta, count=${inBase}.unique.good.filter.count_table, diffs=$preClusterDiffs, processors=1)"

    # remove chimeras
    echo -e "\n"
    echo -e "[MOTHUR] ** chimera detection and filtering **"
    # in general one would use de-novo mode and use pre-cluster sizes for chimera determination, but using a MOCK sample a reference based chimera detection should be more reliable
    # so if a MOCK file is present, do the following
   	${scriptDir}/AppendCountTableToFastA.pl ${inBase}.unique.good.filter.unique.precluster.fasta ${inBase}.unique.good.filter.unique.precluster.count_table > ${inBase}.unique.good.filter.unique.precluster.count.fasta
    ${usearch} -sortbysize ${inBase}.unique.good.filter.unique.precluster.count.fasta -fastaout ${inBase}.unique.good.filter.unique.precluster.count.fasta.sorted -minsize 1 
	mv ${inBase}.unique.good.filter.unique.precluster.count.fasta.sorted ${inBase}.unique.good.filter.unique.precluster.count.fasta
    if [ "${useUCHIMEREF}" == "1" ]; then
		# for the reference database make sure that all references are present in both directions as currently uchime only supports search on forward oriented sequences (option -strand plus is mandatory)
		#${mothur} "#set.logfile(name=mothur_logs/mothur.uchime.log);chimera.uchime(fasta=${inBase}.unique.good.filter.unique.precluster.fasta, reference=${referenceC}, dereplicate=t, processors=$numProc)"
		$usearch -uchime_ref ${inBase}.unique.good.filter.unique.precluster.count.fasta -threads $numProc -strand plus -db ${referenceC} -chimeras ${inBase}.unique.good.filter.unique.precluster.uchime.accnos -nonchimeras ${inBase}.unique.good.filter.unique.precluster.pick.fasta -uchimeout ${inBase}.unique.good.filter.unique.precluster.uchime.chimeras 
    else
		$usearch -uchime_denovo ${inBase}.unique.good.filter.unique.precluster.count.fasta -threads $numProc -chimeras ${inBase}.unique.good.filter.unique.precluster.uchime.accnos -nonchimeras ${inBase}.unique.good.filter.unique.precluster.pick.fasta -uchimeout ${inBase}.unique.good.filter.unique.precluster.uchime.chimeras 
		#${mothur} "#set.logfile(name=mothur_logs/mothur.uchime.log);chimera.uchime(fasta=${inBase}.unique.good.filter.unique.precluster.fasta, count=${inBase}.unique.good.filter.unique.precluster.count_table, dereplicate=t, processors=$numProc)"
    fi
    #${mothur} "#set.logfile(name=mothur_logs/mothur.uchime.remove.log);remove.seqs(fasta=${inBase}.unique.good.filter.unique.precluster.fasta, accnos=${inBase}.unique.good.filter.unique.precluster.uchime.accnos)"
	# now we need to remove the appended count information on the fasta fiel again, so that the IDs match between usearch and mothur
	${scriptDir}/RemoveCountFromFasta.pl ${inBase}.unique.good.filter.unique.precluster.pick.fasta > ${inBase}.unique.good.filter.unique.precluster.pick.fasta_post
	mv ${inBase}.unique.good.filter.unique.precluster.pick.fasta_post ${inBase}.unique.good.filter.unique.precluster.pick.fasta
    ${mothur} "#unique.seqs(fasta=${inBase}.unique.good.filter.unique.precluster.pick.fasta, count=${inBase}.unique.good.filter.unique.precluster.count_table)" 
    #rm ${inBase}.unique.good.filter.unique.precluster.pick.unique.fasta


    # classify sequences using 
    echo -e "\n"
    echo -e "[MOTHUR] ** classify sequences using bayesian approch according to reference DB: ${referenceClassFas} and ${referenceClassTax} **"
    ${mothur} "#set.logfile(name=mothur_logs/mothur.classify.silva.log);classify.seqs(fasta=${inBase}.unique.good.filter.unique.precluster.pick.fasta, count=${inBase}.unique.good.filter.unique.precluster.pick.count_table, template=${referenceClassFas}, taxonomy=${referenceClassTax}, cutoff=$classCutOff, processors=$numProc, probs=f)"

    # cluster OTUs
    # clustering with mothur takes to long or doesnt work at all, we will do this with usearch
    # first convert fasta to usearch format
    ${scriptDir}/AppendCountTableToFastA.pl ${inBase}.unique.good.filter.unique.precluster.pick.fasta ${inBase}.unique.good.filter.unique.precluster.pick.count_table > ${inBase}.cheked.count.fasta
    ${scriptDir}/clean_fasta.py -f ${inBase}.cheked.count.fasta
    mv ${inBase%%.highQual}_filtered.fasta ${inBase}.cheked.count.fasta 
    sed -i "-es/^>\(.*\)/>\1;barcodelabel=mock1;/" ${inBase}.cheked.count.fasta
    ${usearch} -sortbysize ${inBase}.cheked.count.fasta -fastaout ${inBase}.cheked.count.sorted.fasta -minsize 2 
    ${usearch} -cluster_otus ${inBase}.cheked.count.sorted.fasta -otus ${inBase}.cheked.count.sorted.OTUs.fasta -uparseout ${inBase}.cheked.count.sorted.OTUs.out -sizein -sizeout
    ${usearch} -usearch_global ${inBase}.cheked.count.fasta -db ${inBase}.cheked.count.sorted.OTUs.fasta -strand plus -id $clusteringPercentIdentCutOff -uc ${inBase}.checked.count.OTUs.map.uc
	${scriptDir}/ConvertUSEARCH2MothurList.pl ${inBase}.checked.count.OTUs.map.uc ${inBase}.count_table ${inBase}.cheked.count.sorted.OTUs.fasta ${inBase}.OTUs.list ${inBase}.OTUs.centroids.fasta



    #classify OTUs
    echo -e "\n"
    echo -e "[MOTHUR] ** classify OTUs based on read level classification **"
    taxonomyFile=`find $PWD -name "*.taxonomy"`
    ${mothur} "#set.logfile(name=mothur_logs/mothur.classify.OTUs.log);classify.otu(list=${inBase}.OTUs.list, count=${inBase}.unique.good.filter.unique.precluster.pick.count_table, taxonomy=${taxonomyFile}, label=$clusteringPercentIdentLabel, probs=f, basis=sequence)"

 	taxHeader=""
    tail -qn +2 ${inBase}.OTUs.0.03.cons.taxonomy | sort -k 1b,1 > ${inBase}.OTUs.0.03.cons.taxTableOTU
    touch ${inBase}.OTUs.0.03.cons.tax_intermediate
    tail -qn +2 ${inBase}.OTUs.0.03.cons.taxonomy | sort -k 1b,1 | join -a1 -a2 -o 2.2 -e "0" ${inBase}.OTUs.0.03.cons.taxTableOTU - | paste ${inBase}.OTUs.0.03.cons.tax_intermediate - > ${inBase}.OTUs.0.03.cons.tax_intermediate2
    mv ${inBase}.OTUs.0.03.cons.tax_intermediate2 ${inBase}.OTUs.0.03.cons.tax_intermediate
    taxHeader="${taxHeader}\tSample"
    echo -e "OTU\tTaxonomy\tTotalSize${taxHeader}" > ${inBase}.OTUs.0.03.cons.taxTableOTU_intermediate
    $cat ${inBase}.OTUs.0.03.cons.taxTableOTU | awk '{print $1,"\t",$3,"\t",$2}' | paste - ${inBase}.OTUs.0.03.cons.tax_intermediate | sort -nr -k 3,3 >> ${inBase}.OTUs.0.03.cons.taxTableOTU_intermediate
    mv ${inBase}.OTUs.0.03.cons.taxTableOTU_intermediate ${inBase}.OTUs.0.03.cons.taxTableOTU
    rm ${inBase}.OTUs.0.03.cons.tax_intermediate

    for level in {1..6}; do
        echo -e "Taxa\tTotalSize${taxHeader}" > ${inBase}.OTUs.0.03_L${level}.taxTableTotal
        $grep "^$level" ${inBase}.OTUs.0.03.cons.tax.summary | $grep -v unclassified | $awk '{all=$3;for(i=5;i<=NF;++i)all=all"\t"$i;print all}' | sort -k 1 >> ${inBase}.OTUs.0.03_L${level}.taxTableTotal
        $grep "^$level" ${inBase}.OTUs.0.03.cons.tax.summary | $grep unclassified | $awk '{all="";for(i=5;i<=NF;++i)all=all"\t"$i;print all}'| $awk '{for(i=1;i<=NF;++i)sum[i]+=$i}END{line="";for(i=1;i<=NF;++i)line=line"\t"sum[i];print "unclassified"line}' >> ${inBase}.OTUs.0.03_L${level}.taxTableTotal
    done

    #diversity analysis
    echo -e "\n"
    echo -e "[MOTHUR] ** calculate diversity metrics **"
    ${mothur} "#set.logfile(name=mothur_logs/mothur.list.seqs.log);list.seqs(list=${inBase}.OTUs.list)"
    $cat ${inBase}.unique.good.filter.unique.precluster.pick.count_table | $awk '{print $1,"\t",$2,"\t",$2}' > ${inBase}.unique.good.filter.unique.precluster.pick.count_table.grouped
    ${mothur} "#set.logfile(name=mothur_logs/mothur.get.seqs.log);get.seqs(accnos=${inBase}.OTUs.accnos, count=${inBase}.unique.good.filter.unique.precluster.pick.count_table.grouped)"
    ${mothur} "#set.logfile(name=mothur_logs/mothur.make.shared.log);make.shared(list=${inBase}.OTUs.list, count=${inBase}.unique.good.filter.unique.precluster.pick.count_table.pick.grouped)"
    ${mothur} "#set.logfile(name=mothur_logs/mothur.collect.single.log);collect.single(list=${inBase}.OTUs.list, calc=chao-ace-shannoneven-shannon-simpson-invsimpson, freq=100)"
    ${mothur} "#set.logfile(name=mothur_logs/mothur.rarefaction.log);rarefaction.single(list=${inBase}.OTUs.list, calc=sobs, freq=100)"
    ${mothur} "#set.logfile(name=mothur_logs/mothur.summary.single.log);summary.single(shared=${inBase}.OTUs.shared, calc=nseqs-coverage-sobs-chao-ace-shannoneven-shannon-simpson-invsimpson)"

    #cleaning up Mothur and preparing TUIT and error analysis 
    echo -e "\n"
    echo -e "[MOTHUR] ** finished mothur analysis. Cleaning up ... **"
    cd ${outDir}
    mkdir ${outDir}/results 
    cd ${outDir}/results 
    cp ${outDir}/mothur_pipeline/${inBase}.OTUs.list ${outPrefix}_OTUs.list
    cp ${outDir}/mothur_pipeline/${inBase}.OTUs.${clusteringPercentIdentLabel}.cons.tax.summary ${outPrefix}_OTUs.tax.summary
    cp ${outDir}/mothur_pipeline/${inBase}.OTUs.${clusteringPercentIdentLabel}.cons.taxonomy ${outPrefix}_OTUs.taxonomy 
    cp ${outDir}/mothur_pipeline/${inBase}.OTUs.shared ${outPrefix}_OTUs.shared
    cp ${outDir}/mothur_pipeline/${inBase}.OTUs.simpson ${outPrefix}_OTUs.simpson
    cp ${outDir}/mothur_pipeline/${inBase}.OTUs.shannoneven ${outPrefix}_OTUs.shannoneven
    cp ${outDir}/mothur_pipeline/${inBase}.OTUs.shannon ${outPrefix}_OTUs.shannon
    cp ${outDir}/mothur_pipeline/${inBase}.OTUs.invsimpson ${outPrefix}_OTUs.invsimpson
    cp ${outDir}/mothur_pipeline/${inBase}.OTUs.chao ${outPrefix}_OTUs.chao
    cp ${outDir}/mothur_pipeline/${inBase}.OTUs.ace ${outPrefix}_OTUs.ace
    cp ${outDir}/mothur_pipeline/${inBase}.OTUs.rarefaction ${outPrefix}_OTUs.rarefaction
    cp ${outDir}/mothur_pipeline/${inBase}.OTUs.groups.summary ${outPrefix}_OTUs.groups.summary

    cp ${outDir}/mothur_pipeline/${inBase}.OTUs.0.03.cons.taxTableOTU ${outPrefix}_OTUs.taxTableOTU
    for level in {1..6}; do
		cp ${outDir}/mothur_pipeline/${inBase}.OTUs.0.03_L${level}.taxTableTotal ${outPrefix}_OTUs_L${level}.taxTableTotal
	done
	
    cd ${outDir}   
##############################################

#### classify sequences using TUIT ####
if [ "${useTUIT}" == "1" ]; then 
    echo -e "\n"
    echo -e "[TUIT] ** TUIT classification enbaaled **"
    echo -e "[TUIT] ** classify sequences using TUIT and reference DB: ${referenceTUIT}**"
    mkdir ${outDir}/tuit
    cd ${outDir}/tuit
    ln -s ${outDir}/mothur_pipeline/${inBase}.unique.good.filter.unique.precluster.pick.fasta 
    ln -s ${outDir}/mothur_pipeline/${inBase}.unique.good.filter.unique.precluster.pick.count_table 
    mkdir ${outDir}/tuit/splitted
    cd ${outDir}/tuit/splitted
    totNums=`$grep -c \> ../${inBase}.unique.good.filter.unique.precluster.pick.fasta`
    seqsPerFile=`perl -e "printf(\"%.0f\", $totNums/$numProc)"`
    echo -e "[TUIT] splitting FASTA file into sub-parts: $seqsPerFile sequences per file = for $numProc total files"
    ${scriptDir}/split_multifasta.pl --input_file ../${inBase}.unique.good.filter.unique.precluster.pick.fasta --output_dir=$PWD --seqs_per_file=$seqsPerFile
    parts=`ls *.fsa`
    echo -e "[TUIT] running BLAST on each splitted FASTA and TUIT afterwards ... "
    echo -e "[TUIT] executing: parallel --gnu ${scriptDir}/run_blast_tuit.sh {} ${referenceTUIT} ${referenceGIL} ${EVALUE} 1 ::: ${parts} >> ${outDir}/tuit/${inBase}.unique.good.filter.unique.precluster.pick.tuit"
    parallel --gnu ${scriptDir}/run_blast_tuit.sh {1} ${referenceTUIT} ${referenceGIL} ${EVALUE} 1 ::: ${parts} >> ${outDir}/tuit/${inBase}.unique.good.filter.unique.precluster.pick.tuit
    echo -e "[TUIT] ... done"
    cd ..
    ${scriptDir}/tuit_output2mothur_readable.sh ${inBase}.unique.good.filter.unique.precluster.pick.tuit ${inBase}.unique.good.filter.unique.precluster.pick.count_table
    cd ${outDir}/results
    cp ${outDir}/tuit/${inBase}.unique.good.filter.unique.precluster.pick.tax ${outPrefix}_TUIT.taxononmy 
    cp ${outDir}/tuit/${inBase}.unique.good.filter.unique.precluster.pick.tax.summary ${outPrefix}_TUIT.tax.summary
    echo -e "[TUIT] finished TUIT. Results are: ${outDir}results/${outPrefix}_TUIT.tax \t \n\n and  ${outDir}results/${outPrefix}_TUIT.tax.summary"
    cd ${outDir}
fi
##########################################

#### perform error analysis if MOCK sample is given ####
if [ -r "${MOCK}" ]; then
    echo -e "\n"
    echo -e "[PROF] ** perform error analysis **"
    mkdir ${outDir}/error
    cd ${outDir}/error
    ln -s ${outDir}/mothur_pipeline/${inBase}.unique.good.filter.unique.precluster.pick.fasta 
    ln -s ${outDir}/mothur_pipeline/${inBase}.unique.good.filter.unique.precluster.pick.count_table 
    if [ ! -e ${MOCK}.bwt ]; then
	echo -e "[PROF] BWA index file of ${MOCK} does not exist. Creating it .."
	$bwa index ${MOCK}
	echo -e "[PROF] ... done."
    fi
    if [ "$isMerged" != "1" ]; then
	#perform error analysis on both files seperated 
        ln -s ${inputF}
        ln -s ${inputR}
        ln -s ${outDir}/${inputFHist}
        ln -s ${outDir}/${inputRHist}

        echo -e "[PROF] calculating read count and max length of input data perform error analysis ..."
        allstats1=`$cat ${inputFHist} | head -n 3 | tr "\n" " "` 
	allstats2=`$cat ${inputRHist} | head -n 3 | tr "\n" " "` 
        n_reads1=`echo $allstats1 | awk '{print $4}'`
	n_reads2=`echo $allstats2 | awk '{print $4}'`
        #n_reads=`bc $allstats1 + $allstats2`
	max_length1=`echo $allstats1 | awk '{print $7}'`
        max_length2=`echo $allstats2 | awk '{print $7}'`
	#max_length=`perl -e 'if ($max_length1>$max_length2) {print "$max_length1";} else {print "$max_length2";}'`
        echo -e "[PROF] ... done. Number of forward reas=$n_reads1, Max length=$max_length1"
        echo -e "[PROF] ... done. Number of reverse reas=$n_reads2, Max length=$max_length2"

	echo -e "[PROF] running forward mapping: $bwa $bwa_params ${MOCK} ${inputF} ${inputR} > mapping.sam"
	$bwa $bwa_params ${MOCK} $inputF > mapping.fwd.sam
	echo -e "[PROF] running reverse mapping: $bwa $bwa_params ${MOCK} ${inputF} ${inputR} > mapping.sam"
	$bwa $bwa_params ${MOCK} $inputR > mapping.rev.sam
    
	echo -e "[PROF] Filter unique: ${scriptDir}/filter_unique mapping.fwd.sam ${n_reads1} > mapping.fwd.unique.sam"
	${scriptDir}/filter_unique mapping.fwd.sam ${n_reads1} > mapping.fwd.unique.sam
	echo -e "[PROF] Filter unique: ${scriptDir}/filter_unique mapping.rev.sam ${n_reads2} > mapping.rev.unique.sam"
	${scriptDir}/filter_unique mapping.rev.sam ${n_reads2} > mapping.rev.unique.sam

	mv mapping.fwd.unique.sam mapping.fwd.sam
	mv mapping.rev.unique.sam mapping.rev.sam

        echo -e "[PROF] running forward samtools: calmd -e -S mapping.fwd.sam ${MOCK} > mapping.fwd.sam.calmd"
	$samtools calmd -e -S mapping.fwd.sam ${MOCK} > mapping.fwd.sam.calmd
        echo -e "[PROF] running freverse samtools: calmd -e -S mapping.rev.sam ${MOCK} > mapping.rev.sam.calmd"
	$samtools calmd -e -S mapping.rev.sam ${MOCK} > mapping.rev.sam.calmd

        samF=`realpath mapping.fwd.sam`
	calmdF=`realpath mapping.fwd.sam.calmd`
        samR=`realpath mapping.rev.sam`
	calmdR=`realpath mapping.rev.sam.calmd`

	outPrefixF=`basename ${inputF}`
	outPrefixR=`basename ${inputR}`

        echo -e "[PROF] calculate the error profiles running ... "
	echo -e "[PROF] executing forward: ${scriptDir}/ErrorProfiles_wQ_v1.0.sh errorProfile_${outPrefix} $numProc ${calmdF} ${samF} $max_length1"
        ${scriptDir}/ErrorProfiles_wQ_v1.0.sh errorProfile_${outPrefixF} $numProc ${calmdF} ${sam} $max_length1
	echo -e "[PROF] executing reverse: ${scriptDir}/ErrorProfiles_wQ_v1.0.sh errorProfile_${outPrefix} $numProc ${calmdR} ${samR} $max_length2"
        ${scriptDir}/ErrorProfiles_wQ_v1.0.sh errorProfile_${outPrefixR} $numProc ${calmdR} ${sam} $max_length2
	echo -e "[PROF] ... done."
	cp errorProfile_${outPrefixF}/errorProfile_${outPrefixF}.table ${outDir}/results
	cp errorProfile_${outPrefixR}/errorProfile_${outPrefixR}.table ${outDir}/results
    fi
    ln -s ${inputM}
    histM=`basename ${inputM}`.hist
    echo -e "[PROF] calculating max length of input data perform error analysis ..."
    $perl ${scriptDir}/FastaStats.pl -q ${inputM} > ${histM}
    allstats=`cat ${histM} | head -n 3 | tr "\n" " "` 
    n_reads=`echo $allstats | awk '{print $4}'`
    max_length=`echo $allstats | awk '{print $7}'`
    echo -e "[PROF] ... done. Max length=$max_length"
	
    echo -e "[PROF] running mapping: $bwa $bwa_params ${MOCK} ${inputM}  > mapping.sam"
    $bwa $bwa_params ${MOCK} $inputM > mapping.sam

    echo -e "[PROF] Filter unique: ${scriptDir}/filter_unique mapping.sam ${n_reads} > mapping.unique.sam"
    ${scriptDir}/filter_unique mapping.sam ${n_reads} > mapping.unique.sam
    mv mapping.unique.sam mapping.sam
    echo -e "[PROF] running samtools: calmd -e -S mapping.sam ${MOCK} > mapping.sam.calmd"
    $samtools calmd -e -S mapping.sam ${MOCK} > mapping.sam.calmd
    sam=`realpath mapping.sam`
    calmd=`realpath mapping.sam.calmd`

    echo -e "[PROF] calculate the error profiles running ... "
    echo -e "[PROF] executing: ${scriptDir}/ErrorProfiles_wQ_v1.0.sh errorProfile_${outPrefix} $numProc ${calmd} ${sam} $max_length"
    ${scriptDir}/ErrorProfiles_wQ_v1.0.sh errorProfile_${outPrefix} $numProc ${calmd} ${sam} $max_length
    echo -e "[PROF] ... done."
    cp errorProfile_${outPrefix}/errorProfile_${outPrefix}.table ${outDir}/results
    cd ${outDir}
fi
##########################################


 
echo -e "\n\n\n [FINISHED] finished complete amplicon pipe. All Results are written to: ${outDir}/results"

exit 1;
