#!/bin/bash

export PATH="/vol/qiime-1.9/anaconda-2.1x64/bin:$PATH"
source activate qiime1_9_1

pick_open_reference_otus.py -i ~/workdir/split_libraries_fastq_out/seqs.fna -o ~/workdir/open_ref_otu10/ -a -O 12 -m usearch61 -v --min_otu_size 10 --suppress_step4
