#!/usr/bin/perl

sub revcomp{
    my $dna = shift;
    my $revcomp = reverse($dna);
    $revcomp =~ tr/ABCDGHMNRSTUVWXYabcdghmnrstuvwxy/TVGHCDKNYSAABWXRtvghcdknysaabwxr/;
    return $revcomp;
}

my $seqIn="";
while(<>){
    chomp;
    $seqIn.=$_;
}
print STDOUT revcomp($seqIn);
