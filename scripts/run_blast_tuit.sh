#! /bin/bash
# this script starts blast and processes the output via TUIT, then removes the blast output and returns the TUIT result to STDOUT
INPUT=$1
DB=$2
GIL=$3
EVALUE=$4
PROC=$5

#done
blastn -query $INPUT -db $DB -gilist $GIL -evalue $EVALUE -num_threads 1 -outfmt 5 -out ${INPUT}.out > /dev/null 2>&1
java -Xmx12048M -jar /usr/local/tuit/tuit/tuit.jar -p /usr/local/tuit/tuit/properties.xml -i $INPUT -o ${INPUT}.tuit -b ${INPUT}.out > /dev/null 2>&1
cat ${INPUT}.tuit 
#rm ${INPUT}.out
#rm ${INPUT}.tuit
