#!/bin/bash

inDir=$1
outDir=$2
workDir=$inDir

declare -a results=(
grouped.checked.count.OTUs.map.uc
grouped.cheked.count.fasta
grouped.cheked.count.sorted.fasta
grouped.cheked.count.sorted.OTUs.fasta
grouped.cheked.count.sorted.OTUs.out
grouped.fasta
grouped.OTUs.0.03.cons.taxonomy
grouped.OTUs.0.03.cons.tax.summary
grouped.OTUs.0.03.cons.taxTableOTU
grouped.OTUs.0.03.heatmap.bin.svg
grouped.OTUs.0.03.jest.heatmap.sim.svg
grouped.OTUs.0.03_L1.taxTableTotal
grouped.OTUs.0.03_L2.taxTableTotal
grouped.OTUs.0.03_L3.taxTableTotal
grouped.OTUs.0.03_L4.taxTableTotal
grouped.OTUs.0.03_L5.taxTableTotal
grouped.OTUs.0.03_L6.taxTableTotal
grouped.OTUs.0.03.thetayc.heatmap.sim.svg
grouped.OTUs.accnos
grouped.OTUs.centroids.fasta
grouped.OTUs.jabund
grouped.OTUs.jclass
grouped.OTUs.jclass.0.03.lt.dist
grouped.OTUs.jclass.0.03.tre
grouped.OTUs.jest
grouped.OTUs.list
grouped.OTUs.shared
grouped.OTUs.shared.ace
grouped.OTUs.shared.chao
grouped.OTUs.shared.rarefaction
grouped.OTUs.shared.sobs
grouped.OTUs.sorabund
grouped.OTUs.sorclass
grouped.OTUs.sorest
grouped.OTUs.summary
grouped.OTUs.thetan
grouped.OTUs.thetayc
grouped.OTUs.thetayc.0.03.lt.dist
grouped.OTUs.thetayc.0.03.tre
grouped.pick.count_table
grouped.taxonomy
grouped.unique.fasta
../GROUPS
)
for i in "${results[@]}"
do
	echo -e "cp ${workDir}/$i ${outDir}"
	cp ${workDir}/$i ${outDir}
done

