#! /bin/bash
  
function usage {
    echo -e "\n  THIS script starts one of the amplicon MiSeq EASY pipelines while iterating over an input lists which are giving the input data pairs and the used primers"
    echo -e "\n  Usage: $0 -i <input_list> -p <path_to_single_sample_results> -s <path_to_script> -o <output_dir>" 
    echo -e "\n"
    echo -e "\t-i  : the input list file that was used for the single sample analysis"
    echo -e "\t-d  : the directory where the single sample results are sored (base outdir)"
    echo -e "\t-o  : the output directory"
    echo -e "\t-n  : the name of the output file"
}

binDir="/vol/metaphylomic/usr/bin"

while getopts 'i:d:o:s:h' opt
do
   case $opt in
    i) input=$OPTARG;;
    d) singleDir=$OPTARG;;
    o) outDir=$OPTARG;;
    s) outName=$OPTARG;;
    h) usage;;
    \?)
        echo "Invalid option: -$OPTARG" >&2
        exit 1
        ;;
    :)
        echo "Option -$OPTARG requires an argument." >&2
        exit 1
        ;;
    esac
done

if [ ! -r ${input} ]; then
    echo -e "Wrong input list $input. Aborting!"
    usage;
    exit 1;
fi

input=`readlink -f ${input}`
singleDir=`readlink -f ${singleDir}`
if [ "a${outDir}" != "a" ]; then
    if [ ! -d ${outDir} ]; then
        echo -e "[OUT] Output directory given. Creating output directory: ${outDir}"
    	mkdir ${outDir}
    else
        echo -e "[OUT] Output directory given. Using ${outDir}"
    fi
fi

if [ "a${outName}" == "a" ]; then
	outName="processing.stats.tsv"
fi

output=$outDir/${outName}

echo -e "sample\treads\tmeanQF\tmeanQR\tcombined_pairst\tmeanQC\tprimers_removed\tq_clipped\ttrimmed\tmeanQHQ\tuniq\taligned\talignedDC\tdenoised\tdenoisedDC\tchim_free\tchim_freeDC\tperc_merged\tperc_HQ\tperc_Ali\tperc_chimfree" >> ${output}

counter=1
while read inLine; do
    inA=`echo $inLine | awk '{print $1}'`
    inB=`echo $inLine | awk '{print $2}'`
    echo -e "[$counter] Processing the $counter line of $input"
    # get the pre-processed fasta file for the multi-sample analysis
    base1=`basename $inA`
    base2=`basename $inB`
	meanQF=`${binDir}/fastx_quality_stats -i $inA  | awk '{ total += $6 } END { print total/NR }'`
	meanQR=`${binDir}/fastx_quality_stats -i $inB  | awk '{ total += $6 } END { print total/NR }'`
    outbase=${base1%.*}_${base2%.*}
    outdir=`readlink -f ${singleDir}/${base1%.*}_${base2%.*}`
    echo -e "[$counter] Searching for single-run output in $outdir ..."
    if [ ! -d ${outdir} ]; then
        echo -e "Something went wrong in the pipeline $script. Output dir $outdir was not created but expected. Aborting!"
        exit 1;
    fi
	# Collect now sample statistics
	# sample
	reads=`grep -a "Total pairs" ${singleDir}/${base1%.*}_${base2%.*}.log | head -n 1 | awk '{print $4}'`
	combined_pairs=`grep -a "Combined pairs" ${singleDir}/${base1%.*}_${base2%.*}.log | awk '{print $4}' | paste -sd+ | bc `
	meanQC=`${binDir}/fastx_quality_stats -i ${outdir}/merged.fastq | awk '{ total += $6 } END { print total/NR }'`
	primers_removed=`grep -a "Total FastQ records" ${singleDir}/${base1%.*}_${base2%.*}.log | awk '{print $4}'`
	q_clipped=`grep -a "FastQ records kept" ${singleDir}/${base1%.*}_${base2%.*}.log | awk '{print $4}'`
	trimmed=`grep -c -E "^\+$" ${outdir}/merged.highQual.fastq`	
	meanQHQ=`${binDir}/fastx_quality_stats -i  ${outdir}/merged.highQual.fastq | awk '{ total += $6 } END { print total/NR }'`
	unique=`grep -c \> ${outdir}/mothur_pipeline/merged.highQual.unique.fasta`	
	aligned=`grep -c \> ${outdir}/mothur_pipeline/merged.highQual.unique.good.filter.unique.fasta`	
	alignedDC=`cat ${outdir}/mothur_pipeline/merged.highQual.unique.good.filter.count_table | awk '{s+=$NF}END{print s}'`	
	denoised=`grep -c \> ${outdir}/mothur_pipeline/merged.highQual.unique.good.filter.unique.precluster.fasta`	
	denoisedDC=`cat ${outdir}/mothur_pipeline/merged.highQual.unique.good.filter.unique.precluster.count_table | awk '{s+=$NF}END{print s}'`	
	chim_free=`grep -c \> ${outdir}/mothur_pipeline/merged.highQual.unique.good.filter.unique.precluster.pick.unique.fasta`	
	chim_freeDC=`cat ${outdir}/mothur_pipeline/merged.highQual.unique.good.filter.unique.precluster.pick.count_table | awk '{s+=$NF}END{print s}'`
	perc_merged=`perl -e "{printf(\"%.2f\", ($combined_pairs/$reads)*100)}"`
	perc_HQ=`perl -e "{printf(\"%.2f\", ($trimmed/$reads)*100)}"`
	perc_ali=`perl -e "{printf(\"%.2f\", ($alignedDC/$trimmed)*100)}"`
	perc_chimfree=`perl -e "{printf(\"%.2f\", ($chim_freeDC/$denoisedDC)*100)}"`
	echo -e "$outbase\t$reads\t$meanQF\t$meanQR\t$combined_pairs\t$meanQC\t$primers_removed\t$q_clipped\t$trimmed\t$meanQHQ\t$unique\t$aligned\t$alignedDC\t$denoised\t$denoisedDC\t$chim_free\t$chim_freeDC\t$perc_merged\t$perc_HQ\t$perc_ali\t$perc_chimfree" >> ${output}
    echo -e "[$counter] ... done."
    counter=$(($counter+1))
done < $input

