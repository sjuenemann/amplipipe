#!/bin/bash

inplist=${1}
outbase=${2}

database=""
bwa_params=""
threads=20
basedir="/vol/ngscomparison/mock/analysis/error_analysis"
bwa=$basedir/bwa
echo "Running on host: $HOSTNAME"

if [ ! -e ${inplist} ]; then
    echo "Input File ${inplist} not present. Aborting."
    exit 1;
fi
input_line=`sed -n ${SGE_TASK_ID}p ${inplist}`
input=`echo "${input_line}" | awk '{printf("%s",$1)}'`
count=`echo "${input_line}" | awk '{printf("%s",$2)}'`

infile=`realpath -s ${input}`
input=`basename ${input}`
if [[ -n $count ]]; then
	countfile=`realpath -s ${count}`
fi

echo "Processing ${input}."
cd ${outbase}

if [[ $input == *"V1V9"* ]]; then
	database="/vol/ngscomparison/mock/database/mock_bwa/mock_large.fasta"
	bwa_params="mem -t $threads -x pacbio -h 0"
elif [[ $input == *"V1V8"* ]]; then
	database="/vol/ngscomparison/mock/database/mock_bwa/mock_small.fasta"
	bwa_params="mem -t $threads -x pacbio -h 0"
else
	database="/vol/ngscomparison/mock/database/mock_bwa/mock_mis.fasta"
	bwa_params="mem -t $threads -h 0"
fi

if [[ ! -e output_${input%%.fastq} ]]; then
	echo -e "Creating: mkdir output_${input%%.fastq}"
	mkdir output_${input%%.fastq}
fi

allstats=`${basedir}/FastaStats.pl -q ${infile} | head -n 3 | tr "\n" " "` 
n_reads=`echo $allstats | awk '{print $4}'`
max_length=`echo $allstats | awk '{print $7}'`
echo -e "Reads: $n_reads, Max length: $max_length"

echo -e "Switching to: output_${input%%.fastq}"
cd output_${input%%.fastq}

echo -e "Running: $bwa $bwa_params $database $infile > ${input%%.fastq}.sam"
$bwa $bwa_params $database $infile > ${input%%.fastq}.sam
echo -e "Filter unique: ${basedir}/filter_unique ${input%%.fastq}.sam ${n_reads} > ${input%%.fastq}.unique.sam"
${basedir}/filter_unique ${input%%.fastq}.sam ${n_reads} > ${input%%.fastq}.unique.sam
mv ${input%%.fastq}.unique.sam ${input%%.fastq}.sam
echo -e "Running: samtools calmd -e -S ${input%%.fastq}.sam $database > ${input%%.fastq}.sam.calmd"
samtools calmd -e -S ${input%%.fastq}.sam $database > ${input%%.fastq}.sam.calmd
sam=`realpath ${input%%.fastq}.sam`
calmd=`realpath ${input%%.fastq}.sam.calmd`

if [[ -n $count ]]; then
	echo -e "running: /vol/ngscomparison/mock/analysis/error_analysis/bin/ErrorProfiles_wQ_v1.0.sh errorProfile_${input%%.fastq} $threads ${calmd} ${sam} $max_length $countfile"
	/vol/ngscomparison/mock/analysis/error_analysis/bin/ErrorProfiles_wQ_v1.0.sh errorProfile_${input%%.fastq} $threads ${calmd} ${sam} $max_length $countfile
else
	echo -e "running: /vol/ngscomparison/mock/analysis/error_analysis/bin/ErrorProfiles_wQ_v1.0.sh errorProfile_${input%%.fastq} 30 ${calmd} ${sam} $max_length"
	/vol/ngscomparison/mock/analysis/error_analysis/bin/ErrorProfiles_wQ_v1.0.sh errorProfile_${input%%.fastq} $threads ${calmd} ${sam} $max_length
fi

