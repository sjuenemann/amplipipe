#! /usr/bin/perl -w

# this script removes the appended count info on the fasta header as added buy  the AppendCount scipt

use strict;
use warnings;
use Data::Dumper;

my ($file, $uchime) = @ARGV;

my $tag = ($uchime && $uchime ne "" ) ? '^>(\S+.*)/ab=\d+/$' : '^>(\S+.*);size=\d+;$';


print readFastaData($file);
sub readFastaData{
    my $file = shift;
    my $counter = 0;    

    my $data = ""; 
    my $name = "";
    my $sequence = "";
    die "Unable to open file: $file" unless (-r $file);
    open (FASTA, "$file")|| warn "Could not open $file:$!\n";
    my $h1; my $s; my $h2; my $q;
    while (<FASTA>) {
	chomp;
        if (/$tag/) {
			if ($name) {
            	$data .= sprintf(">%s\n%s\n", $name,$sequence);
            }
            $name = $1;
            $sequence = "";
        }
        else {
            $sequence .= $_;
        }
    }
    if ($name) {
   		$data .= sprintf(">%s\n%s\n", $name,$sequence);
    }
    close(FASTA);
    return $data;
}
