#! /bin/bash
# this script takes the output from tuit (rdp style) and modifies it such that mothur summary.tax command can read it

cp $1 $1.backup
sed -i "s/domain\|phylum\|class\t\|order\|family\|genus\|species//g" $1
sed -i "s/\t0.95\|\t0//g" $1
sed -i "s/\t\t/\t/g" $1
sed -i "s/\t/;/g" $1
sed -i "s/;Bacteria/\tBacteria/" $1
sed -i "s/ /_/g" $1
#sed -i "s/\(^.*\)\t/\L\1\t/" $1
/usr/local/bin/mothur "#summary.tax(taxonomy=$1, count=$2)"

grep -E "^7" ${1%.*}.tax.summary | grep -v unclassified | awk '{print $5,$3}' | sort -n -r -k1 > ${1%.*}.tax 
unclassified=`grep -E "^7" ${1%.*}.tax.summary | grep unclassified | awk '{print $5}' | sort -n -r -k1`
unclassified=`echo ${unclassified} | tr ' ' '+' | bc`
echo -E "$unclassified unclassified" >> ${1%.*}.tax
