export LD_LIBRARY_PATH=/vol/pacbio/smrtanalysis/current/common/lib/
export PYTHONPATH=/vol/pacbio/smrtanalysis/current/analysis/lib/python2.7/:/vol/cluster-data/jueneman/Muenster/Resources/local/lib/python2.7/site-packages/
export PATH=$PATH:/vol/cluster-data/jueneman/Muenster/Resources/linux/mothur/:/vol/pacbio/smrtanalysis/current/analysis/bin:/homes/jueneman/Muenster/Resources/linux/PacBio/pbdagcon/src

inlist=(`cat $1`)
basename=/vol/ngscomparison/mock/analysis/error_analysis

for input in ${inlist[@]}
do

if [[ $input == *"V1V9"* ]]; then
	database="/vol/metaphylomic/MOCK/22mock_PacBio_v1-v9_variants/PacBio_v1v9_fasta-files/MOCK.both_strands.fasta"
elif [[ $input == *"V1V8"* ]]; then
	database="/vol/metaphylomic/MOCK/22mock_PacBio_variants/PacBio_Fasta-files/MOCK.both_strands.fasta"
else
    database="/vol/ngscomparison/mock/database/mock_bwa/mock_miseq.fasta"
fi

input=`basename $input`

#$basename/fastq2fasta.pl -a ${input}
/vol/cmg/bin/usearch7.0.1090_i86linux64  -uchime_ref ${input%%.*}.fa -db $database -uchimeout ${input%%.fastq}.uchime -strand plus
grep -E "[Y]$" ${input%%.fastq}.uchime | awk '{print "@"$2}' >  ${input%%.fastq}.chims
perl ${basename}/FilterFastqById.pl ${input} ${input%%.fastq}.chims 1 0 > ${input%%.fastq}.free.fastq
mv ${input%%.fastq}.free.fastq ${input}
rm ${input%%.fastq}.uchime
rm ${input%%.fastq}.chims
done


