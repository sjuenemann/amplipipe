#! /usr/bin/perl -w

#this script takes a fasta file an a mothur generated count_table and incorporates the counts to the fasta header resulting in a usearch compatible format (;size=XX;)

use strict;
use warnings;
use Data::Dumper;

my ($file, $count_file, $fasta_file, $out_list, $out_centroids, $label) = @ARGV;

open (COUNT, $count_file) or die;
my $size = 0;
my $okIDs = {};
while (<COUNT>) {
    my ($line, $rest) = $_ =~ m/^([^\t]+)\t(\d+).*/;
    $okIDs->{$line} = $rest if ($line && $rest);
}
close COUNT;

open (IN, $file) or die;
my $counter = 0;
my $OTUs = {};
my $OTUsize = {};
my $OTUcounts = {};
while (<IN>) {
    chomp;
    my ($ok, $id, $ref) = $_ =~ m/^([^\t]+)\t.*\t([^\t;]+).*\t([^\t;]+).*/;
    if($ok ne "N") {
	$OTUs->{$ref} = [] unless $OTUs->{$ref};
	$OTUsize->{$ref} = 0 unless $OTUsize->{$ref};
	$OTUcounts->{$ref} = 0 unless $OTUcounts->{$ref};
	if($id eq $ref) {
	    unshift(@{$OTUs->{$ref}}, $id);
	    $OTUsize->{$ref}++;
	    $OTUcounts->{$ref} = $OTUcounts->{$ref} + $okIDs->{$ref};
	    $counter++;
	}else{
	    push(@{$OTUs->{$ref}}, $id);
	    $OTUsize->{$ref}++;
	    $OTUcounts->{$ref} = $OTUcounts->{$ref} + $okIDs->{$id};
	}
    }
}
close IN;

my $sequence_of;
my $read_id = "";
my $seq = "";
open (FASTA, $fasta_file) or die;
while (my $line = <FASTA>) {
    chomp $line;
    # read defline
    if ($line =~ /^>/) {
        # Load up the previous sequence if there was one.
        if ( ($read_id) && ($seq) ) { $sequence_of->{$read_id} = $seq; }
        $seq = "";  # reinitialize the sequence

        # Get the new read id
        $read_id = $line;
        $read_id =~ s/^>//; # remove > from the beginning
        $read_id =~ s/\s+.*$//;  #take only the first word
        $read_id =~ s/;.*$//;  #remove anything after the first ";" (including the  ";") \
    } else {
        $seq .= $line;
    }
}
# Load up the last sequence if there was one.
if ( ($read_id) && ($seq) ) { $sequence_of->{$read_id} = $seq; }
close FASTA;

#$label = "0.03" if $label="";

#format digital leading 0s
my $digits=length($counter);
my $format_string="OTU%0".$digits."u\t";
my $format_string2=">OTU%0".$digits."u;orig=%s;size=%d;count=%d;\n%s\n";

open(OUT_CENT, ">$out_centroids");
open(OTU_LIST, ">$out_list");
print OTU_LIST "label\tnumOtus\t";
printf OTU_LIST ($format_string, $_) foreach (1..$counter);
print OTU_LIST "\n0.03\t$counter\t";
my $count=0;
foreach my $key (sort{$OTUcounts->{$b} <=>$OTUcounts->{$a}} keys %$OTUcounts){
#foreach my $key (keys(%$OTUs)){
    print STDERR "$key ::".$OTUsize->{$key}." :: ". $OTUcounts->{$key}."\n";
    print OTU_LIST join(",",@{$OTUs->{$key}})."\t";
	printf OUT_CENT $format_string2,++$count,$key,$OTUsize->{$key},$OTUcounts->{$key}, $sequence_of->{$key} ;
}
print OTU_LIST "\n";
close OTU_LIST;
close OUT_CENT;
