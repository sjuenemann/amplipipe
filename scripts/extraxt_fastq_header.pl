#!/usr/bin/env perl 

#just reports the header line by line

use strict;
use warnings;
use Data::Dumper;

my $printFullHeader = $ARGV[1] || 0;
my $operateFasta = $ARGV[2] || 0;
my $step = ($operateFasta)? 2 : 4;

open ( IN, $ARGV[0] ) or die;
my $counter = 0;
while (<IN>) {
    if ( ($counter%$step) == 0 ) {
	my ($line, $rest) = $_ =~ m/^([@|>]\S+)(.*)/;
	if ($printFullHeader) {
	    print "$line $rest\n";
	} else {
	    print $line."\n";
        }
    }
    $counter++;
}
close IN;
