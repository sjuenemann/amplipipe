#! /usr/bin/env perl -w

use strict;
use warnings;
use Data::Dumper;

my ($file, $idList, $useOnlyFirstID, $filterInclude) = @ARGV;

# useOnlyFirstID = is true, only the first ID in the idList will be used (seperated by space)
# filterInclude = if true, the ids in iDList will be included  instead of beeing filtered out

my $okIDs = {};
if ($idList) {
    open (IN2 , '<', $idList) or die $!;
    while (my $line = <IN2>) {
	chomp($line);
	if ($useOnlyFirstID) {
	    my @parts = split (" ", $line);
	    $line = $parts[$useOnlyFirstID-1];
	}
	$okIDs->{$line} = 1;
    }
}
close(IN2);
print readFastaData($file);
sub readFastaData{
    my $file = shift;
    my $counter = 0;    

    my $data = ""; 
    my $name = "";
    my $sequence = "";
    die "Unable to open file: $file" unless (-r $file);
    open (FASTA, "$file")|| warn "Could not open $file:$!\n";
    my $h1; my $s; my $h2; my $q;
    while ($h1 = <FASTA>) {
	$s = <FASTA>;
	$h2 = <FASTA>;
	$q = <FASTA>;
	chomp $h1;
        if (($name) = $h1 =~ /^(@\S+).*$/) {
            if ($name) {
		if ($filterInclude) {
		    if ($okIDs->{$name})  {
			$data .= $h1."\n".$s.$h2.$q;
		    }
		}else {
		    if ($okIDs->{$name})  {
		    }
		    else{
			$data .= $h1."\n".$s.$h2.$q;
		    }
		}
            }
	}
    }
    close(FASTA);
    return $data;
}
