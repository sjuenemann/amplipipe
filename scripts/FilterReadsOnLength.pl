#!/usr/bin/env perl 

use strict;
use warnings;
use Data::Dumper;

my ($file, $length_min, $length_max) = @ARGV;

my ($ok, $notOK) = readFastaData($file);

print $ok;
print STDERR $notOK;

sub readFastaData{
    my $file = shift;
    
    my $data = ""; 
    my $error ="";
    my $name = "";
    my $sequence = "";
    die "Unable to open file: $file" unless (-r $file);
    open (FASTA, "$file")|| warn "Could not open $file:$!\n";
    while (<FASTA>) {
        chomp;
        if (/^>(\S+.*)$/) {
            if ($name) {
		if (length($sequence) > $length_max || length($sequence) < $length_min){
		    $error .= sprintf(">%s\n%s\n", $name, $sequence) if ($name ne "" && $sequence ne "");
		    #$data .= sprintf("%s\n", $name) if ($name ne "" && $sequence ne "");
		}else {
		    $data .= sprintf(">%s\n%s\n", $name, $sequence) if ($name ne "" && $sequence ne "");
		}
            }
            $name = $1;
            $sequence = "";
        }
        else {
            $sequence .= $_;
        }
    }
    if ($name) {
	if (length($sequence) > $length_max || length($sequence) < $length_min){
	    $error .= sprintf(">%s\n%s\n", $name, $sequence) if ($name ne "" && $sequence ne "");
	}else {
	    $data .= sprintf(">%s\n%s\n", $name, $sequence) if ($name ne "" && $sequence ne "");
	}
    }
    close(FASTA);
    return ($data, $error);

}

