#! /bin/bash
file=$1
cp $file ${file}.bak
real=`readlink $1`
cat $file | awk '{gsub("^$","NA;NA;NA;NA;NA;NA;NA;",$2);} {print $1"\t"$2}' > $file.tmp
mv $file.tmp $real
