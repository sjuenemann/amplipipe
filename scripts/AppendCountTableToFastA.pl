#! /usr/bin/perl -w

#this script takes a fasta file an a mothur generated count_table and incorporates the counts to the fasta header resulting in a usearch compatible format (;size=XX;)

use strict;
use warnings;
use Data::Dumper;

my ($file, $file2, $uchime) = @ARGV;

my $tag = ($uchime && $uchime ne "" ) ? "/ab=%d/" : ";size=%d;";


open (IN, $file2) or die;
my $counter = 0;
my $okIDs = {};
while (<IN>) {
	
    my ($line, $rest) = $_ =~ m/^([^\t]+)\t(\d+).*/;
    $okIDs->{$line} = $rest if ($line && $rest);
}
close IN;

print readFastaData($file);
sub readFastaData{
    my $file = shift;
    my $counter = 0;    

    my $data = ""; 
    my $name = "";
    my $sequence = "";
    die "Unable to open file: $file" unless (-r $file);
    open (FASTA, "$file")|| warn "Could not open $file:$!\n";
    my $h1; my $s; my $h2; my $q;
    while (<FASTA>) {
	chomp;
    if (/^>(\S+).*$/) {
		if ($name) {
                if ($okIDs->{$name}){
                    $data .= sprintf(">%s$tag\n%s\n", $name, $okIDs->{$name},$sequence);
                }else {
                    print STDERR "ERROR: for $name no count_table entry exists.\n";
                }
            }
            $name = $1;
            $sequence = "";
        }
        else {
            $sequence .= $_;
        }
    }
    if ($name) {
	if ($okIDs->{$name}){
	    $data .= sprintf(">%s$tag\n%s\n", $name, $okIDs->{$name},$sequence);
        }else {
            print STDERR "ERROR: for $name no count_table entry exists.\n";
        }
    }
    close(FASTA);
    return $data;
}
